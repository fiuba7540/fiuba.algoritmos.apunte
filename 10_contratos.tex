\chapter{Documentación, contratos y mutabilidad}

En esta unidad se le dará cierta formalización a algunos temas que habían sido
presentados informalmente, como por ejemplo la documentación de las funciones.

Se formalizarán las condiciones que debe cumplir un algoritmo al comenzar, en
su transcurso, y al terminar, y algunas técnicas para tener en cuenta estas
condiciones.

También se verá una forma de modelar el espacio donde \textit{viven} las
variables.

\section{Documentación}

Comenzamos formalizando un poco más acerca de la documentación, cuál es su
objetivo y las distintas formas de documentar.

\subsection{Comentarios vs documentación}

En Python tenemos dos convenciones diferentes para documentar nuestro código:
la \emph{documentación} propiamente dicha (lo que ponemos entre \verb|"| o
\verb|"""| al principio de cada función o módulo), y los \emph{comentarios}
(\verb|#|).  En la mayoría de los lenguajes de programación hay convenciones
similares. ¿Por qué tenemos dos formas diferentes de documentar?

La \emph{documentación} tiene como objetivo explicar \emph{qué} hace el código.
La documentación está dirigida a cualquier persona que desee utilizar la
función o módulo, para que pueda entender cómo usarla sin necesidad de leer el
código fuente.  Esto es útil incluso cuando quien implementó la función es la
misma persona que la va a utilizar, ya que permite separar responsabilidades.

Los \emph{comentarios} tienen como objetivo explicar \emph{cómo} funciona el
código, y \emph{por qué} se decidió implementarlo de esa manera. Los comentarios
están dirigidos a quien esté leyendo el código fuente.

Podemos ver la diferencia entre la documentación y los comentarios en la
función |elegir_codigo| de nuestra implementacion del juego Mastermind
(Código~\ref{cod:mastermind}):

\begin{codigo-python-sn}
def elegir_codigo():
    """Devuelve un codigo de 4 digitos elegido al azar"""
    digitos = ('0','1','2','3','4','5','6','7','8','9')
    codigo = ''
    for i in range(4):
        candidato = random.choice(digitos)
        # Debemos asegurarnos de no repetir digitos
        while candidato in codigo:
            candidato = random.choice(digitos)
        codigo = codigo + candidato
    return codigo
\end{codigo-python-sn}

\subsection{¿Por qué documentamos?}

Seamos sinceros: nadie quiere escribir documentación. ¿Para qué repetir con
palabras lo que ya está estipulado en el código? La documentación es algo que
muy a menudo se deja \emph{para después}, y cuando llega el tan angustioso
momento de escribirla, lo que se termina haciendo es escribir lo más
escueto posible que pueda pasar como \enquote{documentación}.

Incluso es muy frecuente que durante el desarrollo de un proyecto el código
evolucione con el tiempo, pero que nos olvidemos de actualizar la documentación
para reflejar los cambios. En este caso no solamente tenemos documentación de
mala calidad, ¡sino que además es incorrecta!

Pese a todo esto, la realidad sigue siendo que una buena documentación es
componente esencial de cualquier proyecto exitoso. Esto en parte se debe a que
el código fuente transmite en detalle las operaciones individuales que componen
un algoritmo o programa, pero no suele transmitir en forma transparente cosas
como la \emph{intención} del programa, el \emph{diseño} de alto nivel, las
\emph{razones} por las que se decidió utilizar un algoritmo u otro, etc.

\subsection{Código autodocumentado}

En teoría, si nuestro código pudiera transmitir en forma eficiente todos esos
conceptos, la documentación sería menos necesaria. De hecho, existe una técnica de
programación llamada \emph{código autodocumentado}, en la que la idea principal
es elegir los nombres de funciones y variables de forma tal que la
documentación sea innecesaria.

Tomemos como ejemplo el siguiente código:

\begin{codigo-python-sn}
a = 9.81
b = 5
c = 0.5 * a * b**2
\end{codigo-python-sn}

Leyendo esas tres líneas de código podemos razonar cuál será el valor final de
las variables |a|, |b| y |c|, pero no hay nada que nos indique qué representan
esas variables, o cuál es la intención del código. Una opción para mejorarlo sería
utilizar comentarios para aclarar la intención:

\begin{codigo-python-sn}
a = 9.81   # Valor de la constante G (aceleración gravitacional), en m/s²
b = 5      # Tiempo en segundos
c = 0.5 * a * b**2  # Desplazamiento (en metros)
\end{codigo-python-sn}

Otra opción, según la técnica de código autodocumentado, es simplemente asignar
nombres descriptivos a las variables:

\begin{codigo-python-sn}
aceleracion_gravitacional = 9.81
tiempo_segundos = 5
desplazamiento_metros = 0.5 * aceleracion_gravitacional * tiempo_segundos ** 2
\end{codigo-python-sn}

De esta manera logramos que no sea necesario ningún comentario ni documentación
adicional, ya que la intención del código es mucho más descriptiva.

La técnica de código autodocumentado presenta varias limitaciones. Entre ellas:

\begin{itemize}
    \item Elegir buenos nombres es una tarea difícil, que
        requiere tener en cuenta cosas como: qué tan descriptivo es el nombre
        (cuanto más, mejor), la longitud del identificador
        (cuanto más corto mejor), el alcance del identificador (cuánto más
        grande, más descriptivo debe ser el nombre), y convenciones (|i| para
        índices, |c| para caracteres, etc).
    \item La documentación de todas formas termina siendo necesaria, ya que por
        muy bien que elijamos los nombres, muchas veces la única forma de
        explicar la intención del código y todos sus detalles es en lenguaje
        coloquial.
    \item En ciertos contextos sigue siendo deseable, o imprescindible, que
        quien quiera utilizar nuestra función o módulo pueda entender su
        funcionamiento sin necesidad de leer el código fuente.
\end{itemize}

\subsection{Un error común: la sobredocumentación}

Si bien la ausencia de documentación suele ser perjudicial, el otro extremo
también lo es: la \emph{sobredocumentación}. Después de todo, en la vida
diaria no necesitamos carteles que nos recuerden cosas como \enquote{esta es la
puerta}, \enquote{este es el picaporte} y \enquote{empujar hacia abajo para abrir}. De
la misma manera, podríamos decir que el siguiente código peca de ser sobredocumentado:

\begin{codigo-python-sn}
def buscar_elemento(lista_de_numeros, numero):
    """Esta función devuelve el índice (contando desde 0) en el que se
       encuentra el número `numero` en la lista `lista_de_numeros`.
       Si el elemento no está en la lista devuelve -1.
    """
    # Recorremos todos los índices de la lista, desde 0 (inclusive) hasta N
    # (no inclusive)
    for indice in range(len(lista_de_numeros)):
        # Si el elemento en la posicion `indice` es el buscado
        if lista_de_numeros[indice] == numero:
            # Devolvemos el índice en el que está el elemento
            return indice
    # No lo encontramos, devolvemos -1
    return -1
\end{codigo-python-sn}

Algunas cosas que podemos mejorar:

\begin{itemize}
\item En la firma de la función los nombres |buscar_elemento|,
    |lista_de_numeros| y |numero| se pueden simplificar a |buscar|, |secuencia| y
    |elemento|. Cambiamos |lista_de_numeros| por |secuencia|, ya que la función
    puede recibir secuencias de cualquier tipo, con elementos de cualquier
    tipo, y no hay ninguna razón para limitar a que sea una lista de números.
\item Las variable interna |indice| también se puede simplificar:
    por convención podemos usar |i|.
\item \enquote{Esta función} es redundante: cuando alguien lea la documentación ya va
    a saber que se trata de una función.
\item \enquote{contando desde 0} es redundante: en Python siempre contamos desde 0.
\item Los comentarios son excesivos: la función es suficientemente simple y
    cualquier persona que sepa programación básica podrá entender el algoritmo.
\end{itemize}

Corrigiendo todos estos detalles resulta:

\begin{codigo-python-sn}
def buscar(lista, elemento):
    """Devuelve el índice en el que se encuentra el `elemento` en la `lista`,
       o -1 si no está.
    """
    for i in range(len(lista)):
        if lista[i] == elemento:
            return i
    return -1
\end{codigo-python-sn}

\section{Contratos}

Cuando hablamos de \textit{contratos} o \textit{programación por
contratos}, nos referimos a la necesidad de estipular tanto lo que necesita
como lo que devuelve nuestro código. El contrato de una función suele ser
incluido en su documentación.

Algunos ejemplos de cosas que deben ser estipuladas como parte del contrato
son: cómo deben ser los parámetros recibidos, cómo va a ser lo que se devuelve,
y si la función provoca algún efecto secundario (como por ejemplo modificar
alguno de los parámetros recibidos o imprimir algo en la consola).

Algunas de estas condiciones deben estar dadas antes de ejecutar el código o
función; a estas condiciones las llamamos \emph{precondiciones}. Si se cumplen
las precondiciones, habrá un conjunto de condiciones sobre el estado en que
quedan las variables y el o los valores de retorno una vez finalizada la
ejecución, que llamamos \emph{postcondiciones}.

\subsection{Precondiciones}

Las precondiciones de una función son las condiciones que deben cumplirse antes
de ejecutarla, para que se comporte correctamente: cómo deben ser los
parámetros que recibe, cómo debe ser el estado global, etc.

Por ejemplo, en una función que divide dos números, las precondiciones son que los parámetros
son números, y que el divisor es distinto de 0.

Si estipulamos las precondiciones como parte de la documentación, en el cuerpo
de la función podremos asumir que son ciertas, y no será necesario escribir
código para lidiar con los casos en los que no se cumplen.

\subsection{Postcondiciones}

Las postcodiciones son las condiciones que se cumplirán una vez finalizada la
ejecución de la función (asumiendo que se cumplen las precondiciones): cómo
será el valor de retorno, si los parámetros recibidos o variables globales son
alteradas, si se imprimen cosas, si se modifican archivos, etc.

En el ejemplo anterior, la función división, dadas las precondiciones
puede asegurar que devolverá un número correspondiente al cociente solicitado.

\subsection{Aseveraciones}

Tanto las precondiciones como las postcondiciones son \textit{aseveraciones}
(en inglés \textit{assertions}). Es decir, afirmaciones realizadas en un momento
particular de la ejecución sobre el estado computacional. Si llegaran a ser
falsas significaría que hay algún error en el diseño o utilización del algoritmo.

En algunos casos puede ser útil comprobar estas afirmaciones desde el código, y
para ello podemos utilizar la instrucción \lstinline!assert!. Esta instrucción
recibe una condición a verificar (o sea, una expresión booleana).
Si la condición es |True|, la instrucción no hace nada; en caso contrario se
produce un error.

\begin{codigo-python-sn}
>>> assert True
>>> assert False
(^Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AssertionError^)
\end{codigo-python-sn}

Opcionalmente, la instrucción |assert| puede recibir
un mensaje de error que mostrará en caso que la condición no se cumpla.

\begin{codigo-python-sn}
>>> n = 0
>>> assert n != 0, "El divisor no puede ser 0"
(^Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AssertionError: El divisor no puede ser 0^)
\end{codigo-python-sn}

\begin{atencion}
Es importante tener en cuenta que \lstinline!assert! está pensado para ser
usado en la etapa de desarrollo. Un programa terminado nunca debería dejar
de funcionar por este tipo de errores.
\end{atencion}

\subsection{Ejemplos}

Usando los ejemplos anteriores, la función \lstinline!division! nos
quedaría de la siguiente forma:

\begin{codigo-python-sn}
def division(dividendo, divisor):
    """Cálculo de la división

    Pre: Recibe dos números, divisor debe ser distinto de 0.
    Post: Devuelve un número real, con el cociente de ambos.
    """
    assert divisor != 0, "El divisor no puede ser 0"
    return dividendo / divisor
\end{codigo-python-sn}

Otro ejemplo, tal vez más interesante, puede ser una función que implemente
una sumatoria ($\sum_{i=inicial}^{final} f(i)$).  En este caso hay que
analizar cuáles van a ser los parámetros que recibirá la función, y las
precondiciones que estos parámetros deberán cumplir.

La función |sumatoria| a escribir necesita de un valor inicial, un valor
final, y una función a la cual llamar en cada paso. Es decir que recibe
tres parámetros.

\begin{codigo-python-sn}
def sumatoria(inicial, final, f):
\end{codigo-python-sn}

Tanto \lstinline!inicial! como \lstinline!final! deben ser números enteros,
y dependiendo de la implementación a realizar o de la especificación
previa, puede ser necesario que \lstinline!final! deba ser mayor o igual a
\lstinline!inicial!.

Con respecto a \lstinline!f!, se trata de una función que será llamada con
un parámetro en cada paso y se requiere poder sumar el resultado, por lo
que debe ser una función que reciba un número y devuelva un número.

La declaración de la función queda, entonces, de la siguiente manera.

\begin{codigo-python-sn}
def sumatoria(inicial, final, f):
    """Calcula la sumatoria desde i=inicial hasta final de f(i)

    Pre: inicial y final son números enteros, f es una función que
         recibe un entero y devuelve un número.
    Post: Se devuelve el valor de la sumatoria de aplicar f a cada
          número comprendido entre inicial y final.
    """
\end{codigo-python-sn}

\begin{ejercicio}
Realizar la implementación correspondiente a la función \lstinline!sumatoria!.
\end{ejercicio}

En definitiva, la estipulación de pre y postcondiciones dentro de la
documentación de las funciones es una forma de especificar claramente el
comportamiento del código.  Las pre y postcondiciones son, en efecto, un
\textit{contrato} entre el código invocante y el invocado.

\section{Invariantes de ciclo}

% TODO: conseguir frase, la vida es siempre igual, siempre está cambiando.
%\begin{quote}
%\enquote{Dadme un punto de apoyo y moveré el mundo} Arquímedes
%\end{quote}

\label{invariantes}
Los invariantes se refieren a estados o situaciones que no cambian dentro
de un contexto o porción de código.  Hay invariantes de ciclo, que son los
que veremos a continuación, e invariantes de estado, que se verán más
adelante.

El invariante de ciclo permite conocer cómo llegar desde las precondiciones
hasta las postcondiciones, cuando la implementación se compone de un ciclo.
El invariante de ciclo es, entonces, una
aseveración que debe ser verdadera al comienzo de cada iteración.

Por ejemplo, si el problema es ir desde el punto $A$ al punto $B$, las
precondiciones dicen que estamos parados en $A$ y las postcondiciones que
estamos parados en $B$, un invariante podría ser \enquote{estamos en algún punto entre
$A$ y $B$, en el punto más cercano a $B$ que estuvimos hasta ahora}.

Más específicamente, si analizamos el ciclo para buscar el máximo en una lista
desordenada, la precondición es que la lista contiene elementos que son
comparables y la postcondición es que se devuelve el elemento máximo de la
lista.

\begin{codigo-python-sn}
def maximo(lista):
    "Devuelve el elemento máximo de la lista o None si está vacía."
    if not lista:
        return None
    max_elem = lista[0]
    for elemento in lista:
        if elemento > max_elem:
            max_elem = elemento
    return max_elem
\end{codigo-python-sn}

En este caso, el invariante del ciclo es que \lstinline!max_elem! contiene el
valor máximo de la porción de lista analizada.

Los invariantes son de gran importancia al momento de demostrar que un
algoritmo funciona, pero aún cuando no hagamos una demostración formal es muy
útil tener los invariantes a la vista, ya que de esta forma es más fácil
entender cómo funciona un algoritmo y encontrar posibles errores.

Los invariantes, además, son útiles a la hora de determinar las condiciones
iniciales de un algoritmo, ya que también deben cumplirse para ese caso.  Por
ejemplo, consideremos el algoritmo para obtener la potencia \lstinline!n! de
un número.

\begin{codigo-python-sn}
def potencia(b, n):
    "Devuelve la potencia n del número b, con n entero mayor que 0."
    p = 1
    for i in range(n):
        p *= b
    return p
\end{codigo-python-sn}

En este caso, el invariante del ciclo es que la variable \lstinline!p!
contiene el valor de la potencia correspondiente a esa iteración. Teniendo en
cuenta esta condición, es fácil ver que \lstinline!p! debe comenzar el ciclo
con un valor de 1, ya que ese es el valor correspondiente a $p^0$.

De la misma manera, si la operación que se quiere realizar es sumar todos los
elementos de una lista, el invariante será que una variable \lstinline!suma!
contenga la suma de todos los elementos ya recorridos, por lo que es claro que
este invariante debe ser 0 cuando aún no se haya recorrido ningún elemento.

\begin{codigo-python-sn}
def suma(lista):
    "Devuelve la suma de todos los elementos de la lista."
    suma = 0
    for elemento in lista:
        suma += elemento
    return suma
\end{codigo-python-sn}

% TODO
% \subsection{Invariantes como medida de cuánto falta}

%Dependiendo del problema y las herramientas con las que contemos algunos
%invariantes se pueden medir retomando el ejemplo de ir de A a B, uno podría
%medir la distancia hasta B para esta, pero si para medir la distancia hay que ir hasta B
%y volver deja de tener sentido. O al estar buscando el mínimo en una
%secuencia, cómo hago para comprobar que paso a paso tengo el mínimo de la
%secuencia que ya recorrí sin usar

\subsection{Comprobación de invariantes desde el código}

Cuando la comprobación necesaria para saber si seguimos \enquote{en camino} es simple,
se la puede tener directamente dentro del código.  Evitando seguir avanzando
con el algoritmo si se produjo un error crítico.

Por ejemplo, en una búsqueda binaria, el elemento a buscar debe ser mayor que
el elemento inicial y menor que el elemento final, de no ser así, no tiene sentido
continuar con la búsqueda.  Es posible, entonces, agregar una instrucción
que compruebe esta condición y de no ser cierta realice alguna acción para
indicar el error, por ejemplo, utilizando la instrucción \lstinline!assert!,
vista anteriormente.

\section{Mutabilidad e Inmutabilidad}
\label{mutabilidad}

Hasta ahora cada vez que estudiamos un tipo de datos indicamos si son
mutables o inmutables.

Cuando un valor es de un tipo inmutable, como por ejemplo una cadena, es
posible asignar un nuevo valor a esa variable, pero no es posible modificar su
contenido.

\begin{codigo-python-sn}
>>> s = "ejemplo"
>>> s = "otro"
>>> s[2] = "c"
(^Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: 'str' object does not support item assignment^)
\end{codigo-python-sn}

Esto se debe a que cuando se realiza una nueva asignación, no se modifica la
cadena en sí, sino que la variable \lstinline!s! pasa a \emph{referenciar} a otra cadena.
En cambio, no es posible asignar un nuevo caracter en una posición, ya que
esto implicaría modificar la cadena inmutable.

En el caso de los parámetros mutables, la asignación tiene el mismo
comportamiento, es decir que las variables pasan a apuntar a un nuevo valor.

\begin{codigo-python-sn}
>>> lista1 = [10, 20, 30]
>>> lista2 = lista1
>>> lista1 = [3, 5, 7]
>>> lista1
[3, 5, 7]
>>> lista2
[10, 20, 30]
\end{codigo-python-sn}

Algo importante a tener en cuenta en el caso de las variables de tipo
mutable es que si hay dos o más variables que \textit{referencian} a un mismo
valor, y este valor se modifica, el cambio se verá reflejado en ambas variables.

\begin{codigo-python-sn}
>>> lista1 = [1, 2, 3]
>>> lista2 = lista1
>>> lista2[1] = 5
>>> lista1
[1, 5, 3]
\end{codigo-python-sn}

\begin{sabias_que}
En otros lenguajes, como C o C++, existe un tipo de variable especial
llamado \emph{puntero}, que se comporta como una referencia a un valor,
como es el caso de las variables mutables del ejemplo anterior.

En Python no hay punteros como los de C o C++, pero todas las variables son
referencias a una porción de memoria, de modo que cuando se asigna una
variable a otra, lo que se está asignando es la porción de memoria a la que
refieren.  Si esa porción de memoria cambia, el cambio se puede ver en
todas las variables que apuntan a esa porción.
\end{sabias_que}

% TODO: describir en más detalle el modelo de referencia de python ?

\subsection{Parámetros mutables e inmutables}

Las funciones reciben parámetros que pueden ser mutables o inmutables.

Si dentro del cuerpo de la función se modifica uno de estos parámetros para
que \textit{referencie} a otro valor, este cambio no se verá reflejado fuera de la
función.  Si, en cambio, se modifica el \textit{contenido} de alguno de los
parámetros mutables, este cambio \textit{sí} se verá reflejado fuera de la
función.

A continuación un ejemplo en el cual se asigna la variable recibida, a un
nuevo valor.  Esa asignación sólo tiene efecto dentro de la función.

\begin{codigo-python-sn}
>>> def no_cambia_lista(lista):
...     lista = [0, 1, 2, 3]
...     print("Dentro de la funcion lista =", lista)
...
>>> lista = [10, 20, 30, 40]
>>> no_cambia_lista(lista)
Dentro de la funcion lista = [0, 1, 2, 3]
>>> lista
[10, 20, 30, 40]
\end{codigo-python-sn}

A continuación un ejemplo en el cual se modifica la variable recibida. En este
caso, los cambios realizados tienen efecto tanto dentro como fuera de la
función.

\begin{codigo-python-sn}
>>> def cambia_lista(lista):
...     for i in range(len(lista)):
...         lista[i] = lista[i] ** 3
...
>>> lista = [1, 2, 3, 4]
>>> cambia_lista(lista)
>>> lista
[1, 8, 27, 64]
\end{codigo-python-sn}

\begin{atencion}
Por omisión se espera que una función que recibe parámetros mutables no los
modifique, ya que si se los modifica se podría perder información valiosa.

En el caso en que por una decisión de diseño o especificación se modifiquen
los parámetros mutables recibidos, esto debe estar claramente documentado
como parte de las postcondiciones.
\end{atencion}

\section{Resumen}

\begin{itemize}
\item La \textbf{documentación} tiene como objetivo explicar \emph{qué} hace el código,
    y está dirigida a quien desee utilizar la función o módulo.
\item Los \textbf{comentarios} tienen como objetivo explicar \emph{cómo} funciona el
    código y \emph{por qué} se decidió implementarlo de esa manera, y están dirigidos a
    quien esté leyendo el código fuente.
\item El \textbf{contrato} de una función especifica qué condiciones se deben
    cumplir para que la función pueda ser invocada (\textbf{precondiciones}),
    y qué condiciones se garantiza que serán válidas cuando la función termine
    su ejecución (\textbf{postcondiciones}).
\item Los \textbf{invariantes de ciclo} son las condiciones que deben
cumplirse al comienzo de cada iteración de un ciclo.
\item En el caso en que estas \textbf{aseveraciones} no sean verdaderas, se
deberá a un error en el diseño o utilización del código.
\item Si una función modifica un valor mutable que recibe por parámetro, eso
    debe estar explícitamente aclarado en su documentación.
\end{itemize}

\begin{referencia_python}

\begin{sintaxis}{Documentación: \lstinline|"..."| ó \lstinline|"""..."""|}
    Por convención, si la primera línea de una función o módulo es una cadena,
    esa será su documentación
\end{sintaxis}

\begin{sintaxis}{Comentarios: \lstinline|# ...|}
    El intérprete ignora cualquier texto que se encuentra desde el caracter
    |#| hasta el fin de la línea.
\end{sintaxis}

\begin{sintaxis}{\lstinline!assert condicion[, mensaje]!}
Verifica si la condición es verdadera.  En caso contrario, provoca un error
con el mensaje recibido por parámetro.
\end{sintaxis}

\end{referencia_python}

\newpage
\section{Ejercicios}

\extractionlabel{guia}
\begin{ejercicio}
Analizar cada una de las siguientes funciones.
¿Cuál es el contrato de la función? ¿Cómo sería su documentación?
¿Es necesario agregar comentarios?
¿Se puede simplificar el código y/o mejorar su legibilidad?
¿Hay algún invariante de ciclo?

\begin{partes}

\item
\begin{minipage}{0.95\linewidth}
\begin{lstlisting}[numbers=none]
def Abs(i):
    if i >= 0:
        return i
    else:
        return -i
\end{lstlisting}
\end{minipage}

\item
\begin{minipage}{0.95\linewidth}
\begin{lstlisting}[numbers=none]
def emails(diccionario):
    for k, v in diccionario.items():
        print(f"El e-mail de {k} es {v}")
\end{lstlisting}
\end{minipage}

\item
\begin{minipage}{0.95\linewidth}
\begin{lstlisting}[numbers=none]
def emails2(diccionario):
    buenos = {}
    for k, v in diccionario.items():
        if '@' in v:
            buenos[k] = v
    return buenos
\end{lstlisting}
\end{minipage}

\item
\begin{minipage}{0.95\linewidth}
\begin{lstlisting}[numbers=none]
def emails3(nombres, direcciones):
    for nom in range(len(nombres)):
        if direcciones[nom] == None:
            nombre, apellido = ' '.split(nombres[nom].lower())
            direcciones[nom] = nombre[0] + apellido + '@ejemplo.com'
\end{lstlisting}
\end{minipage}

\end{partes}
\end{ejercicio}

\newpage
\begin{subappendices}
\section{Acertijo MU}

El acertijo MU\footnote{%
\url{http://en.wikipedia.org/wiki/Invariant\_(computer\_science)}} es un buen
ejemplo de un problema lógico donde es útil determinar el invariante.  El
acertijo consiste en buscar si es posible convertir MI a MU, utilizando las
siguientes operaciones.

\begin{enumerate}
\item Si una cadena termina con una I, se le puede agregar una U (xI -> xIU)
\item Cualquier cadena luego de una M puede ser totalmente duplicada (Mx ->
Mxx)
\item Donde haya tres Is consecutivas (III) se las puede reemplazar por una U
(xIIIy -> xUy)
\item Dos Us consecutivas, pueden ser eliminadas (xUUy -> xy)
\end{enumerate}

Para resolver este problema, es posible pasar horas aplicando estas reglas
a distintas cadenas.  Sin embargo, puede ser más fácil encontrar una
afirmación que sea invariante para todas las reglas y que muestre si es o
no posible llegar a obtener MU.

Al analizar las reglas, la forma de deshacerse de las Is es conseguir tener
tres Is consecutivas en la cadena.  La única forma de deshacerse de todas las
Is es que haya un cantidad de Is consecutivas múltiplo de tres.

Es por esto que es interesante considerar la siguiente afirmación como
invariante: el número de Is en la cadena no es múltiplo de tres.

Para que esta afirmación sea invariante al acertijo, para
cada una de las reglas se debe cumplir que: si el invariante era verdadero
antes de aplicar la regla, seguirá siendo verdadero luego de aplicarla.

Para ver si esto es cierto o no, es necesario considerar la aplicación del
invariante para cada una de las reglas.

\begin{enumerate}
\item Se agrega una U, la cantidad de Is no varía, por lo cual se mantiene el
invariante.
\item Se duplica toda la cadena luego de la M, siendo $n$ la cantidad de
Is antes de la duplicación, si $n$ no es múltiplo de 3, $2n$ tampoco lo será.
\item Se reemplazan tres Is por una U.  Al igual que antes, siendo $n$ la
cantidad de Is antes del reemplazo, si $n$ no es múltiplo de 3, $n-3$ tampoco
lo será.
\item Se eliminan Us, la cantidad de Is no varía, por lo cual se mantiene el
invariante.
\end{enumerate}

Todo esto indica claramente que el invariante se mantiene para cada una de las
posibles transformaciones.  Esto significa que sea cual fuere la regla que se
elija, si la cantidad de Is no es un múltiplo de tres antes de aplicarla, no
lo será luego de hacerlo.

Teniendo en cuenta que hay una única I en la cadena inicial MI, y que uno no
es múltiplo de tres, es imposible llegar a MU con estas reglas, ya que MU
tiene cero Is, que sí es múltiplo de tres.
\end{subappendices}
