\chapter[Programas sencillos]{Programas sencillos}

En esta unidad empezaremos a resolver problemas sencillos, y a
programarlos en Python.

\section{Construcción de programas}

Cuando nos disponemos a escribir un programa debemos seguir una cierta cantidad
de pasos para asegurarnos de que tendremos éxito en la tarea. La acción
irreflexiva (me siento frente a la computadora y escribo rápidamente y sin
pensar lo que me parece que es la solución) no constituye una actitud
profesional (e ingenieril) de resolución de problemas. Toda construcción tiene
que seguir una metodología, un protocolo de desarrollo.

Existen muchas metodologías para construir programas, pero en este
curso aplicaremos una sencilla, que es adecuada para
la construcción de programas pequeños, y que se puede resumir en
los siguientes pasos:

\begin{enumerate}
\item {\bf Analizar el problema.} Entender profundamente \emph{cuál} es
el problema que se trata de resolver, incluyendo el contexto en el
cual se usará.

\begin{observacion}
Una vez analizado el problema, asentar el análisis por escrito.
\end{observacion}

\item {\bf Especificar la solución.} Éste es el punto en el cual
se describe \emph{qué} debe hacer el programa, sin importar
el cómo. En el caso de los problemas sencillos que abordaremos,
deberemos decidir cuáles son los datos de entrada que se nos
proveen, cuáles son las salidas que debemos producir, y cuál es la
relación entre todos ellos.

\begin{observacion}
Al especificar el problema a resolver, documentar la especificación por
escrito.
\end{observacion}

\item {\bf Diseñar la solución.} Éste es el punto en el cuál
atacamos el \emph{cómo} vamos a resolver el problema, cuáles son
los algoritmos y las estructuras de datos que usaremos. Analizamos
posibles variantes, y las decisiones las tomamos usando como dato
de la realidad el contexto en el que se aplicará la solución, y
los costos asociados a cada diseño.

\begin{observacion}
Luego de diseñar la solución, asentar por escrito el diseño, asegurándonos de
que esté completo.
\end{observacion}

\item {\bf Implementar el diseño.} Traducir a un lenguaje de
programación (en nuestro caso, y por el momento, Python) el diseño
que elegimos en el punto anterior.

\begin{observacion}
La implementación también se debe documentar, con comentarios
dentro y fuera del código, al respecto de qué hace el programa, cómo lo hace y
por qué lo hace de esa forma.
\end{observacion}

\item {\bf Probar el programa.} Diseñar un conjunto de pruebas
para probar cada una de sus partes por separado, y también la
correcta integración entre ellas. Utilizar la \emph{depuración} como
instrumento para descubir dónde se producen ciertos errores.

\begin{observacion}
Al ejecutar las pruebas, documentar los resultados obtenidos.
\end{observacion}

\item {\bf Mantener el programa.} Realizar los cambios en
respuesta a nuevas demandas.

\begin{observacion}
Cuando se realicen cambios, es necesario documentar el análisis,
la especificación, el diseño, la implementación y las pruebas que surjan para
llevar estos cambios a cabo.
\end{observacion}

\end{enumerate}

\section{Realizando un programa sencillo}

Al leer un artículo en una revista norteamericana que contiene información de
longitudes expresadas en millas, pies y pulgadas, queremos poder convertir esas
distancias de modo que sean fáciles de entender.  Para ello, decidimos escribir
un programa que convierta las longitudes del sistema inglés al sistema métrico
decimal.

Antes de comenzar a programar, utilizamos la guía de la sección anterior, para
analizar, especificar, diseñar, implementar y probar el problema.

\begin{enumerate}
\item {\bf Análisis del problema.} En este caso el problema es
sencillo: nos dan un valor expresado en millas, pies y pulgadas y
queremos transformarlo en un valor en el sistema métrico decimal.
Sin embargo hay varias respuestas posibles, porque no hemos fijado
en qué unidad queremos el resultado. Supongamos que decidimos que
queremos expresar todo en metros.

\item {\bf Especificación.} Debemos establecer la relación entre
los datos de entrada y los datos de salida. Ante todo debemos
averiguar los valores para la conversión de las unidades básicas.
Buscando en Internet encontramos la siguiente tabla:

\begin{itemize}
\item 1 milla = 1.609344 km
\item 1 pie = 30.48 cm
\item 1 pulgada = 2.54 cm
\end{itemize}

\begin{atencion}
A lo largo de todo el curso usaremos punto decimal,
en lugar de coma decimal, para representar valores no enteros,
dado que esa es la notación que utiliza Python.
\end{atencion}

La tabla obtenida no traduce las longitudes a metros. La manipulamos para
llevar todo a metros:

\begin{itemize}
\item 1 milla = 1609.344 m
\item 1 pie = 0.3048 m
\item 1 pulgada = 0.0254 m
\end{itemize}

Si una longitud se expresa como $L$ millas, $F$ pies y $P$ pulgadas, su
conversión a metros se calculará como:

$$
M = 1609.344 * L + 0.3048 * F + 0.0254 * P
$$

Hemos especificado el problema. Pasamos entonces a la próxima etapa.

\item {\bf Diseño.} La estructura de este programa es sencilla:
leer los datos de entrada, calcular la solución, mostrar el
resultado, o \emph{Entrada-Cálculo-Salida}.

Antes de escribir el programa, escribiremos en \emph{pseudocódigo}
(un castellano preciso que se usa para describir lo que hace un
programa) una descripción del mismo:

\begin{codigo-nohl-sn}
Leer cuántas millas tiene la longitud dada
 (y referenciarlo con la variable millas)

Leer cuántos pies tiene la longitud dada
 (y referenciarlo con la variable pies)

Leer cuántas pulgadas tiene la longitud dada
 (y referenciarlo con la variable pulgadas)

Calcular metros = 1609.344 * millas +
    0.3048 * pies + 0.0254 * pulgadas

Mostrar por pantalla la variable metros
\end{codigo-nohl-sn}

\item {\bf Implementación.} Ahora estamos en condiciones de
traducir este pseudocódigo a un programa en lenguaje Python:

\begin{codigo}{ametrico.py}{Convierte medidas inglesas a sistema metrico}
\begin{codigo-python}
def main():
    print("Convierte medidas inglesas a sistema metrico")

    millas = int(input("Cuántas millas?: "))
    pies = int(input("Y cuántos pies?: "))
    pulgadas = int(input("Y cuántas pulgadas?: "))

    metros = 1609.344 * millas + 0.3048 * pies + 0.0254 * pulgadas
    print("La longitud es de ", metros, " metros")

main()
\end{codigo-python}
\end{codigo}

\begin{nota}
En nuestra implementación decidimos dar el nombre |main| a la función principal
del programa. Esto no es más que una convención: \enquote{main} significa
\enquote{principal} en inglés.
\end{nota}

\item {\bf Prueba.} Probaremos el programa con valores para los que conocemos
la solución:

\begin{itemize}
\item 1 milla, 0 pies, 0 pulgadas (el resultado debe ser 1609.344 metros).
\item 0 millas, 1 pie, 0 pulgada (el resultado debe ser 0.3048 metros).
\item 0 millas, 0 pies, 1 pulgada (el resultado debe ser 0.0254 metros).
\end{itemize}

La prueba la documentaremos con la sesión de Python
correspondiente a las tres invocaciones a \lstinline!ametrico.py!.
\end{enumerate}

En la sección anterior hicimos hincapié en la necesidad de
documentar todo el proceso de desarrollo. En este ejemplo la
documentación completa del proceso lo constituye todo lo escrito
en esta sección.

\section {Piezas de un programa Python}
Cuando empezamos a hablar en un idioma extranjero es posible que nos entiendan
pese a que cometamos errores. No sucede lo mismo con los lenguajes de
programación: la computadora no nos entenderá si nos desviamos un poco de
alguna de las reglas.

Por eso es que para poder empezar a programar en Python es necesario conocer los elementos
que constituyen un programa en dicho lenguaje y las reglas para construirlos.


\subsection{Nombres}
Ya hemos visto que se usan nombres para denominar a los programas
(\lstinline!ametrico!) y para denominar a las funciones dentro de un
módulo (\lstinline!main!). Cuando queremos dar nombres a valores usamos
variables (\lstinline!millas!, \lstinline!pies!, \lstinline!pulgadas!,
\lstinline!metros!). Todos esos nombres se llaman \emph{identificadores}
y Python tiene reglas sobre qué es un identificador válido y qué
no lo es.

Un identificador comienza con una letra o con guión bajo (\_) y
luego sigue con una secuencia de letras, números y guiones bajos.
Los espacios no están permitidos dentro de los identificadores.

Los siguientes son todos identificadores válidos de Python:

\begin{itemize}[noitemsep]
\item \lstinline!hola!
\item \lstinline!hola12t!
\item \lstinline!_hola!
\item \lstinline!Hola!
\end{itemize}

Python distingue mayúsculas de minúsculas, así que \lstinline!Hola! es
un identificador y \lstinline!hola! es otro identificador.

\begin{observacion}
Por convención, no usaremos identificadores que empiezan con mayúscula.
\end{observacion}

Los siguientes son todos identificadores inválidos de Python:

\begin{itemize}[noitemsep]
\item \lstinline!hola a12t!
\item \lstinline!8hola!
\item \lstinline!hola\%!
\item \lstinline!Hola*9!
\end{itemize}

Python reserva 31 palabras para describir la estructura del
programa, y no permite que se usen como identificadores. Cuando en
un programa nos encontramos con que un nombre no es admitido pese
a que su formato es válido, seguramente se trata de una de las
palabras de esta lista, a la que llamaremos de \emph{palabras
reservadas}. Esta es la lista completa de las palabras reservadas de
Python:

\begin{codigo-nohl-sn}
False      class      finally    is         return
None       continue   for        lambda     try
True       def        from       nonlocal   while
and        del        global     not        with
as         elif       if         or         yield
assert     else       import     pass
break      except     in         raise
\end{codigo-nohl-sn}

\subsection{Expresiones}
Una \emph{expresión} es una porción de código Python que produce o
calcula un \emph{valor} (resultado).

\begin{itemize}
\item La expresión más sencilla es un valor \emph{literal}.  Por ejemplo,
    la expresión |12345| produce el valor numérico 12345.

\item Una expresión puede ser una \emph{variable}, y el valor que produce es el
    que tiene asociada la variable en el estado. Por ejemplo, si |x| $\ra$ 5 en
    el estado, entonces el resultado de la expresión |x|  es el valor 5.

\item Usamos \emph{operaciones} para combinar expresiones y construir
expresiones más complejas:

\begin{itemize}
\item Si \lstinline!x! es como antes, \lstinline!x + 1! es una expresión cuyo
resultado es 6.

\item Si en el estado \lstinline!millas! $\ra$ 1, \lstinline!pies! $\ra$ 0 y
\lstinline!pulgadas! $\ra$ 0, entonces
\lstinline[breaklines=true]!1609.344 * millas + 0.3048 * pies + 0.0254 * pulgadas! es una
expresión cuyo resultado es 1609.344.

\item La exponenciación se representa con el símbolo \lstinline!**!. Por
ejemplo, \lstinline!x**3! significa $x^3$.

\item Se pueden usar paréntesis para indicar un orden de
evaluación: \lstinline[breaklines=true]!((b * b) - (4 * a * c)) / (2 * a)!.

\item Igual que en la notación matemática, si no hay paréntesis en la
expresión, primero se agrupan las exponenciaciones, luego los
productos y cocientes, y luego las sumas y restas.

\item Hay que prestar atención con lo que sucede con los
cocientes:

\begin{itemize}
\item La expresión |6 / 4| produce el valor |1.5|.
\item La expresión |6 // 4| produce el valor |1|, que es el resultado de la
    \emph{división entera} entre 6 y 4.
\item La expresión |6 % 4| produce el valor |2|, que es el
    \emph{resto de la división entera} entre 6 y 4.
\end{itemize}

Como vimos en la sección \ref{punto-flotante}, los números pueden ser tanto
enteros (|0|, |111|, |-24|, almacenados internamente en forma exacta),
como reales (|0.0|, |12.5|, |-12.5|, representados internamente en
forma aproximada como números \emph{de punto flotante}). Dado que los
números enteros y reales se representan de manera diferente, se
comportan de manera diferente frente a las operaciones. En Python, los
números enteros se denominan |int| (de \emph{integer}), y los números
reales |float| (de \emph{floating point}).

\end{itemize}

\item Una expresión puede ser una \emph{llamada a una función}: si |f| es una
    función que recibe un parámetro, y |x| es una variable, la expresión |f(x)|
    produce el valor que devuelve la función |f| al invocarla pasándole el valor
    de |x| por parámetro.

    Algunos ejemplos:

\begin{itemize}
\item |input()| produce el valor ingresado por teclado tal como se lo digita.
\item |abs(x)| produce el valor absoluto del número pasado por parámetro.
\end{itemize}

\end{itemize}

\ejercicioc{%
Aplicando las reglas matemáticas de asociatividad, decidir
cuáles de las siguientes expresiones son iguales entre sí:

\begin{partes}
\item \lstinline!((b * b) - (4 * a * c)) / (2 * a)!
\item \lstinline!((b * b) - (4 * a * c)) // (2 * a)!
\item \lstinline!(b * b - 4 * a * c) / (2 * a)!
\item \lstinline!b * b - 4 * a * c / 2 * a!
\item \lstinline!(b * b) - (4 * a * c / 2 * a)!
\item \lstinline!1 / 2 * b!
\item \lstinline!b / 2!
\end{partes}}

\ejercicioc{Escribir un programa que le asigne a \lstinline!a!, \lstinline!b! y
\lstinline!c!  los valores 10, 100 y 1000 respectivamente y evalúe las
expresiones del ejercicio anterior.}

\ejercicioc{Escribir un programa que le asigne a \lstinline!a!, \lstinline!b! y
\lstinline!c!  los valores 10.0, 100.0 y 1000.0 respectivamente y evalúe las
expresiones del ejercicio anterior.}

\subsection{No sólo de números viven los programas} \label{nosolo}

No sólo tendremos expresiones numéricas en un programa Python.
Recordemos el programa que se usó para saludar a muchos amigos:

\begin{codigo-python-sn}
>>> def hola(alguien):
...     return "Hola " + alguien + "! Estoy programando en Python."
\end{codigo-python-sn}

Para invocar a ese programa y hacer que saludara a Ana había que
escribir \lstinline!hola("Ana")!.
La variable \lstinline!alguien! en dicha invocación queda ligada a un
valor que es una \emph{cadena de caracteres} (letras, dígitos, símbolos,
etc.), en este caso, \lstinline!"Ana"!.

Como en la sección anterior, veremos las reglas de qué constituyen
expresiones con caracteres:

\begin{itemize}
\item Una expresión puede ser simplemente una cadena de texto.  El resultado de
    la expresión literal \lstinline!'Ana'! es precisamente el valor \lstinline!'Ana'!.

\item Una variable puede estar asociada a una cadena de texto: si
    \lstinline!amiga! $\ra$ \lstinline!'Ana'! en el estado, entonces el
    resultado de la expresión \lstinline!amiga! es el valor \lstinline!'Ana'!.

\item Se puede usar comillas simples o dobles para representar cadenas simples:
    |'Ana'| y |"Ana"| son equivalentes.

\item Se puede usar tres comillas (simples o dobles) para representar cadenas
    que incluyen más de una línea de texto:

\begin{codigo-python-sn}
martin_fierro = """Aquí me pongo a cantar
al compás de la vigüela,
que al hombre que lo desvela
una pena estraordinaria,
como el ave solitaria
con el cantar se consuela."""
\end{codigo-python-sn}

\item Usamos operaciones para combinar expresiones y construir
expresiones más complejas, pero atención con qué operaciones están
permitidas sobre cadenas:

\begin{itemize}
    \item El signo \lstinline!+! no representa la suma sino la
        \emph{concatenación} de cadenas: Si \lstinline!amiga! es como antes,
        \lstinline!amiga + 'Laura'!  es una expresión cuyo valor es
        \lstinline!AnaLaura!.

\begin{atencion}
No se puede sumar cadenas con números.
\begin{codigo-python-sn}
>>> amiga="Ana"
>>> amiga+'Laura'
'AnaLaura'
>>> amiga+3
(^Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: cannot concatenate 'str' and 'int' objects^)
>>>
\end{codigo-python-sn}
\end{atencion}

\item El signo \lstinline!*! permite repetir una cadena una cantidad de veces:
    \lstinline!amiga * 3! es una expresión cuyo valor es
    \lstinline!'AnaAnaAna'!.

\begin{atencion}
No se pueden multiplicar cadenas entre sí

\begin{codigo-python-sn}
>>> amiga * 3
'AnaAnaAna'
>>> amiga * amiga
(^Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: can't multiply sequence by non-int of type 'str'^)
\end{codigo-python-sn}
\end{atencion}

\end{itemize}

\end{itemize}

\subsection{Instrucciones}

Las \emph{instrucciones} son las órdenes que entiende Python.  En general cada
línea de un programa Python corresponde a una instrucción.  Algunos ejemplos de
instrucciones que ya hemos utilizado:

\begin{itemize}
\item La instrucción de asignación |<nombre> = <valor>|.

\item La instrucción |return <expresión>|, que provoca que una función devuelva
    el valor resultante de evaluar la expresión.

\item \label{instruccion-expresion} La instrucción más simple que hemos utilizado es la que contiene una
    única |<expresión>|, y el efecto de dicha instrucción es que Python evalúa
    la expresión y descarta su resultado. El siguiente es un programa válido
    en el que todas las instrucciones son del tipo |<expresión>|:

\begin{codigo-python-sn}
0
23.9
abs(-10)
"Este programa no hace nada útil :("
\end{codigo-python-sn}

\end{itemize}

\subsection{Ciclos definidos}
Algunas instrucciones son compuestas, como por ejemplo la instrucción |for|,
que indica a Python que queremos inicializar un \emph{ciclo definido}:

\begin{codigo-python-sn}
for x in range(n1, n2):
    print(x * x)
\end{codigo-python-sn}

Un ciclo definido es de la forma
\begin{codigo-python-sn}
for <nombre> in <expresión>:
    <cuerpo>
\end{codigo-python-sn}

El ciclo |for| es una instrucción compuesta ya que incluye una línea de
inicialización y un |<cuerpo>|, que a su vez está formado por una o más
instrucciones.

Decimos que el ciclo es definido porque una vez evaluada la |<expresión>|
(cuyo resultado debe ser una \emph{secuencia de valores}),
se sabe exactamente cuántas veces se ejecutará
el |<cuerpo>| y qué valores tomará la variable |<nombre>|.

En nuestro ejemplo la secuencia de valores resultante de la expresión
|range(n1, n2)| es el intervalo de enteros |[n1, n1+1, ..., n2-1]| y la variable
es |x|.

La secuencia de valores se puede indicar como:

\begin{itemize}
\item \lstinline!range(n)!. Establece como secuencia de valores a
 \lstinline![0, 1, ..., n-1]!.

\item \lstinline!range(n1, n2)!. Establece como secuencia de valores a
\lstinline![n1, n1+1, ..., n2-1]!.

\item Se puede definir a mano una secuencia entre corchetes. Por ejemplo,
\begin{codigo-python-sn}
for x in [1, 3, 9, 27]:
    print(x * x)
\end{codigo-python-sn}
imprimirá los cuadrados de los números 1, 3, 9 y 27.
\end{itemize}

\section{Una guía para el diseño}

En su artículo \enquote{How to program it}, Simon Thompson plantea
algunas preguntas a sus alumnos que son muy útiles para la etapa
de diseño:

\begin{itemize}
\item ¿Has visto este problema antes, aunque sea de manera
ligeramente diferente?

\item ¿Conoces un problema relacionado? ¿Conoces un programa que
pueda ser útil?

\item Observa la especificación. Intenta encontrar un
problema que te resulte familiar y que tenga la misma
especificación o una parecida.

\item Supongamos que hay un problema relacionado, y que ya fue
resuelto. ¿Puedes usarlo? ¿Puedes usar sus resultados?  ¿Puedes usar sus
métodos? ¿Puedes agregarle alguna parte auxiliar a ese programa del que ya
dispones?

\item Si no puedes resolver el problema propuesto, intenta
resolver uno relacionado. ¿Puedes imaginarte uno relacionado que
sea más fácil de resolver? ¿Uno más general? ¿Uno más específico?
¿Un problema análogo?

\item ¿Puedes resolver una parte del problema?  ¿Puedes sacar algo
útil de los datos de entrada? ¿Puedes pensar qué información es
útil para calcular las salidas? ¿De qué manera se puede manipular
las entradas y las salidas de modo tal que estén \enquote{más cerca}
unas de las otras?

\item ¿Utilizaste todos los datos de entrada? ¿Utilizaste las condiciones
especiales sobre los datos de entrada que aparecen en el
enunciado? ¿Has tenido en cuenta todos los requisitos que se
enuncian en la especificación?

\end{itemize}

\section{Calidad de software}

Los programas que hemos construido hasta ahora son pequeños y simples. Existen
proyectos de software profesionales de tamaños muy diversos, yendo desde
programas sencillos desarrollados por una única persona hasta
proyectos gigantescos, con millones de líneas de código y desarrollados
durante años por miles de personas.

\begin{sabias_que}
Uno de los proyectos de código abierto más colosales es el núcleo
del sistema operativo Linux. Fue publicado por primera vez en 1991, y aun
hoy sigue en desarrollo activo. El código fuente es
público\footnote{\url{https://github.com/torvalds/linux}}, y cualquiera
puede contribuir aportando mejoras. Hasta la versión 4.13 publicada en 2017
participaron más de $15\,000$ personas, creando en total más de 24 millones de
líneas de código.
\end{sabias_que}

Cuanto más grande es un proyecto de software, más difícil es su construcción y
mantenimiento, y más tenemos que prestar atención a la \emph{calidad} con la
que está construido. Presentamos aquí una lista no completa de propiedades que
contribuyen a la calidad, y algunas preguntas que podemos hacer para medir
cuánto contribuye cada factor:

\begin{itemize}
        \item {\bf Confiabilidad:} ¿El sistema resuelve el problema inicial en
            forma correcta? ¿Lo resuelve siempre o a veces falla? ¿Cuántas
            veces falla en un período de tiempo?
        \item {\bf Testabilidad:} ¿Qué tan fácil es probar que el sistema
            funciona correctamente? ¿Hay algún proceso de pruebas automáticas o
            manuales?
        \item {\bf Performance:} ¿Cuánto tarda el sistema en producir un
            resultado? ¿Cuántos recursos consume (memoria, espacio en disco,
            etc.)?
        \item {\bf Usabilidad:} ¿Puede un nuevo usuario aprender a utilizar el
            sistema fácilmente? ¿Las operaciones más comunes son fáciles de
            realizar?
        \item {\bf Mantenibilidad:} ¿Qué tan legible y entendible es el código?
            ¿Qué tan fácil es modificar el comportamiento del programa o
            agregar nuevas funcionalidades?
        \item {\bf Escalabilidad:} ¿Cómo se comporta el sistema cuando
            se incrementa la demanda (cantidad de usuarios, cantidad de datos,
            etc.)?
        \item {\bf Portabilidad:} ¿El sistema puede funcionar en diferentes
            plataformas (arquitecturas de procesador, sistemas operativos,
            navegadores web, etc.)?
        \item {\bf Seguridad:} ¿Los datos sensibles están protegidos de ataques
            informáticos? ¿Qué tan difícil es para un atacante tomar el control,
            desestabilizar o dañar el sistema?
\end{itemize}

Por supuesto, cada proyecto es particular y algunos de las propiedades
mencionadas tendrán más o menos prioridad según el caso. En particular en este
curso nos concentraremos más en que nuestros programas sean confiables y
mantenibles, y también prestaremos atención a la performance (sobre todo al
comparar diferentes algoritmos).

\section{Ejercicios}

\begin{extract*}
En los ejercicios a continuación, utilizar los conceptos de análisis,
especificación y diseño antes de realizar la implementación.
\end{extract*}

\begin{ejercicio} Ciclos definidos
\begin{partes}
	\item Escribir un ciclo definido para imprimir por pantalla
todos los números entre 10 y 20.
	\item Escribir un ciclo definido que salude por pantalla a
sus cinco mejores amigos/as.
	\item Escribir un programa que use un ciclo definido con
rango numérico, que pregunte los nombres de sus cinco mejores
amigos/as, y los salude.
	\item Escribir un programa que use un ciclo definido con
rango numérico, que pregunte los nombres de sus seis mejores
amigos/as, y los salude.
	\item Escribir un programa que use un ciclo definido con
rango numérico, que averigue a cuántos amigos quieren saludar, les
pregunte los nombres de esos amigos/as, y los salude.
\end{partes}
\end{ejercicio}

\extractionlabel{guia}
\begin{ejercicio}
Escribir una función que reciba una cantidad de pesos,
una tasa de interés y un número de años y devuelva el monto
final a obtener.  La fórmula a utilizar es:
\begin{displaymath}
C_n = C \times \left(1+\frac{x}{100}\right)^n
\end{displaymath}
Donde $C$ es el capital inicial, $x$ es la tasa de interés y $n$ es el
número de años a calcular.
\end{ejercicio}

\extractionlabel{guia}
\begin{ejercicio}
Utilizando la función del ejercicio anterior, escribir un programa que le
pregunte al usuario la cantidad de pesos inicial, la tasa de interés y el
número de años y muestre el monto final a obtener.
\end{ejercicio}

\extractionlabel{guia}
\begin{ejercicio}
Escribir una función que convierta un valor dado en grados Fahrenheit a
grados Celsius.  Recordar que la fórmula para la conversión es:
$F = \frac{9}{5}C+32$
\end{ejercicio}

\extractionlabel{guia}
\begin{ejercicio}
Escribir un programa que utilice la función anterior para generar una tabla de conversión de
temperaturas, desde 0 °F hasta 120 °F, de 10 en 10.
\end{ejercicio}

\extractionlabel{guia}
\begin{ejercicio}
\begin{partes}
\item Escribir una función que dado un número entero devuelva 1 si el mismo
es impar y 0 si fuera par.
\item Escribir una función que dado un número entero devuelva 0 si el mismo
es impar y 1 si fuera par.
\item Escribir una función que dado un número entero devuelva el dígito de las unidades.
Por ejemplo, para 153 debe devolver 3.
\item Escribir una función que dado un número devuelva el primer número múltiplo
de 10 inferior a él. Por ejemplo, para 153 debe devolver 150.
\end{partes}
\end{ejercicio}

\extractionlabel{guia}
\begin{ejercicio}
Escribir un programa que imprima todos los números pares entre dos números
que se le pidan al usuario.
\end{ejercicio}

\extractionlabel{guia}
\begin{ejercicio}
Escribir un programa que le pregunte al usuario un número $n$
e imprima los primeros $n$ números triangulares, junto con su
índice. Los números triangulares se obtienen mediante la suma de los números
naturales desde $1$ hasta $n$.  Es decir, si se piden los primeros 5
números triangulares, el programa debe imprimir:

\begin{verbatim}
1 - 1
2 - 3
3 - 6
4 - 10
5 - 15
\end{verbatim}

{\bf Nota}: hacerlo usando y sin usar la ecuación $\sum_{i=1}^n i = n\,(n+1)/2$.
¿Cuál realiza más operaciones?
\end{ejercicio}

\extractionlabel{guia}
\begin{ejercicio}
Escribir un programa que tome una cantidad $m$ de valores ingresados
por el usuario, a cada uno le calcule el factorial (utilizando la función escrita en el ejercicio
\ref{ej:factorial}) e imprima el resultado junto con el número de orden correspondiente.
\end{ejercicio}

\extractionlabel{guia}
\begin{ejercicio}
Escribir un programa que imprima por pantalla todas las fichas de dominó, de
una por línea y sin repetir.
\end{ejercicio}

\extractionlabel{guia}
\begin{ejercicio}
Modificar el programa anterior para que pueda generar fichas de un juego
que puede tener números de 0 a $n$.
\end{ejercicio}
