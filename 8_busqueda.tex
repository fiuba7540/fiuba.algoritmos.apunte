\chapter{Algoritmos de búsqueda}

\section{Búsqueda lineal}

\subsection*{El problema de la búsqueda}

Presentamos ahora uno de los problemas más clásicos de la computación, \emph{el
problema de la búsqueda}, que se puede enunciar de la siguiente manera:

{\bf Problema: } Dada una lista $xs$ y un valor $x$ devolver el índice de $x$
en $xs$ si $x$ está en $xs$, y $-1$ si $x$ no está en $xs$.

Alicia Hacker afirma que este problema tiene una solución muy sencilla en
Python: se puede usar directamente la poderosa función \lstinline+index()+ de
lista.

Probamos esa solución para ver qué pasa:

\begin{codigo-python-sn}
>>> [1, 3, 5, 7].index(5)
2
>>> [1, 3, 5, 7].index(20)
(^Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: list.index(x): x not in list^)
\end{codigo-python-sn}

Vemos que usar la función \lstinline+index()+ resuelve nuestro problema si el
valor buscado está en la lista, pero si el valor no está no sólo no devuelve
un $-1$, sino que se produce un error.

El problema es que para poder aplicar la función \lstinline+index()+ debemos
estar seguros de que el valor está en la lista, y para averiguar eso Python
nos provee del operador \lstinline+in+:

\begin{codigo-python-sn}
>>> 5 in [1, 3, 5, 7]
True
>>> 20 in [1, 3, 5, 7]
False
\end{codigo-python-sn}

O sea que si llamamos a la función \lstinline+index()+ sólo cuando el
resultado de \lstinline+in+ es verdadero, y devolvemos $-1$ cuando el
resultado de \lstinline+in+ es falso, estaremos resolviendo el problema
planteado usando sólo funciones provistas por Python. La solución se plantea a
continuación:

\begin{codigo-python-sn}
def busqueda_con_index(xs, x):
    """Busca un elemento x en una lista xs.

    Si x está en xs devuelve el índice,
    de lo contrario devuelve -1.
    """
    if x in xs:
        return xs.index(x)
    else:
        return -1
\end{codigo-python-sn}

Probamos la función \verb+busqueda_con_index()+:

\begin{codigo-python-sn}
>>> busqueda_con_index([1, 4, 54, 3, 0, -1], 1)
0
>>> busqueda_con_index([1, 4, 54, 3, 0, -1], -1)
5
>>> busqueda_con_index([1, 4, 54, 3, 0, -1], 3)
3
>>> busqueda_con_index([1, 4, 54, 3, 0, -1], 44)
-1
>>> busqueda_con_index([], 0)
-1
\end{codigo-python-sn}

\subsection*{¿Cuántas comparaciones hace este programa?}

Es decir, ¿cuánto esfuerzo computacional requiere
este programa? ¿Cuántas veces compara el valor que buscamos con los datos de
la lista? No lo sabemos porque no sabemos cómo están implementadas las
operaciones \lstinline+in+ e \lstinline+index()+. La pregunta queda planteada
por ahora pero daremos un método para averiguarlo más adelante en esta unidad.

\subsection*{Búsqueda lineal}

Nos interesa ver qué sucede si programamos la búsqueda usando operaciones más
elementales, y no las grandes primitivas \lstinline+in+ e \lstinline+index()+.
Esto nos permitirá estudiar una solución que pueda portarse a otros lenguajes.

Supongamos entonces que en nuestra versión de Python no existen ni \lstinline+in+
ni \lstinline+index()+. Podemos en cambio acceder a cada uno de los elementos
de la lista a través de una construcción \lstinline+for+, y también, por
supuesto, podemos acceder a un elemento de la lista mediante un índice.

Diseñamos la siguiente solución: podemos comparar uno a uno los elementos de la
lista con el valor de \lstinline!x!, y retornar el valor de la posición
donde lo encontramos en caso de encontrarlo.
Si llegamos al final de la lista sin haber salido antes de la función es
porque el valor de \lstinline!x! no está en la lista, y en ese caso
retornamos $-1$.

En esta solución necesitamos una variable \lstinline!i! que cuente en cada
momento en qué posición de la lista estamos parados. Esta variable se
inicializa en $0$ antes de entrar en el ciclo y se incrementa en $1$ en
cada paso.

El programa nos queda entonces como se muestra a continuación:

\begin{codigo-python-sn}
def busqueda_lineal(lista, x):
    """Si x está en lista devuelve su posición en lista, de lo
    contrario devuelve -1.
    """
    i = 0
    for z in lista: (~\circled{1}~)
        if z == x: (~\circled{2}~)
            return i (~\circled{3}~)
        i += 1
    return -1
\end{codigo-python-sn}

Y ahora lo probamos:

\begin{codigo-python-sn}
>>> busqueda_lineal([1, 4, 54, 3, 0, -1], 44)
-1
>>> busqueda_lineal([1, 4, 54, 3, 0, -1], 3)
3
>>> busqueda_lineal([1, 4, 54, 3, 0, -1], 0)
4
>>> busqueda_lineal([], 42)
-1
\end{codigo-python-sn}

\subsection*{¿Cuántas comparaciones hace este programa?}
\label{busqueda-lineal}

Volvemos a preguntarnos lo mismo que en la sección anterior, pero con el nuevo
programa: ¿cuánto esfuerzo computacional requiere este programa?, ¿cuántas
veces compara el valor que buscamos con los datos de la lista? Ahora podemos
analizar el código de \lstinline!busqueda_lineal!:

\begin{itemize}
\item La línea \circled{1} es un ciclo que recorre uno a uno los
elementos de la lista, y en el cuerpo de ese ciclo, en \circled{2} se
compara cada elemento con el valor buscado. En el caso de encontrarlo
(\circled{3}) se devuelve la posición.

\item Si el valor no está en la lista, se recorrerá la lista entera, haciendo
una comparación por cada elemento.
\end{itemize}

O sea que si el valor está en la posición $p$ de la lista se hacen $p$
comparaciones. En el \emph{peor caso}, si el valor no está, se hacen
tantas comparaciones como elementos tenga la lista.

Nuestra hipótesis es: {\bf Si la lista crece, la cantidad de comparaciones
para encontrar un valor arbitrario crecerá en forma proporcional al tamaño de
la lista}. Por lo tanto diremos que:

\begin{observacion}
El algoritmo de búsqueda lineal tiene un comportamiento \emph{proporcional a la
longitud de la lista involucrada}, o que es un algoritmo \emph{lineal}.
\end{observacion}

\section{Búsqueda sobre listas ordenadas}

Si podemos suponer que la lista está previamente ordenada,
¿podemos encontrar una manera más eficiente de buscar elementos sobre ella?

En principio hay una modificación muy simple que podemos hacer sobre el
algoritmo de búsqueda lineal: si estamos buscando el elemento $x$ en una
lista que está ordenada de menor a mayor, en cuanto encontremos algún elemento
mayor a $x$ podemos estar seguros de que $x$ no está en la lista, por lo que no
es necesario continuar recorriendo el resto.

\ejercicioc{Modificar la búsqueda lineal para el caso de listas ordenadas.
En el peor caso, ¿cuál es nuestra nueva hipótesis sobre comportamiento del
algoritmo? ¿Es realmente más eficiente?}

\subsection*{Búsqueda binaria}

¿Podemos hacer algo mejor? Trataremos de aprovechar el hecho de que la lista
está ordenada y vamos a hacer algo distinto: nuestro espacio de búsqueda se
irá achicando a segmentos cada vez menores de la lista original.
La idea es descartar segmentos de la lista donde el valor seguro que no puede
estar:

\begin{enumerate}
\item Consideramos como segmento inicial de búsqueda a la lista completa.

\item Analizamos el punto medio del segmento (el valor central); si es el valor
buscado, devolvemos el índice del punto medio.

\item Si el valor central es mayor al buscado, podemos descartar el segmento
que está desde el punto medio hacia la a derecha.

\item Si el valor central es menor al buscado, podemos descartar el segmento
que está desde el punto medio hacia la izquierda.

\item Una vez descartado el segmento que no nos interesa, volvemos a analizar
el segmento restante, de la misma forma.

\item Si en algún momento el segmento a analizar tiene longitud 0 o negativa
significa que el valor buscado no se encuentra en la lista.
\end{enumerate}

Para señalar la porción del segmento que se está analizando a cada paso,
utilizaremos dos variables (\lstinline!izq! y \lstinline!der!) que
contienen la posición de inicio y la posición de fin del segmento que se
está considerando. De la misma manera usaremos la varible \lstinline!medio!
para contener la posición del punto medio del segmento.

En la Figura~\ref{fig:busqbin} vemos qué pasa cuando se busca
el valor 18 en la lista [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23].

\def\elements{1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23}

\tikzset{pics/arrayrow/.style n args={3}{code={
    \node[anchor=east] at (-1, 0) {#1};
    \foreach \x [count=\i] in \elements {
        \ifthenelse{\x>#2 \AND \x<#3}
            {\node[draw,fill=none,minimum width=0.75cm,minimum height=0.5cm,node font=\ttfamily] at (0.75*\i,0) (\x) {\x};}
            {\node[draw,fill=med-gray,minimum width=0.75cm,minimum height=0.5cm,node font=\ttfamily] at (0.75*\i,0) (\x) {\x};}
    }
}}}
\tikzset{pics/arraymark/.style n args={3}{code={
    \node[below=#3 of #1.south,anchor=north,node font=\ttfamily] (label) {#2};
    \draw[flecha] (label) -- (#1.south);
}}}

\begin{figure}[h!t]
\begin{center}
\begin{tikzpicture}
\pic {arrayrow={El arreglo inicial:}{0}{24}};
\pic {arraymark={1}{izq\,=\,0}{0.25cm}};
\pic {arraymark={11}{medio\,=\,5}{0.25cm}};
\pic {arraymark={23}{der\,=\,11}{0.25cm}};

\pic at (0, -2) {arrayrow={Paso 2 (\texttt{lista[5] < 18}):}{12}{24}};
\pic {arraymark={13}{izq\,=\,6}{0.25cm}};
\pic {arraymark={17}{medio\,=\,8}{0.75cm}};
\pic {arraymark={23}{der\,=\,11}{0.25cm}};

\pic at (0, -4) {arrayrow={Paso 3 (\texttt{lista[8] < 18}):}{18}{24}};
\pic {arraymark={19}{izq\,=\,9}{0.25cm}};
\pic {arraymark={21}{medio\,=\,10}{0.75cm}};
\pic {arraymark={23}{der\,=\,11}{0.25cm}};

\pic at (0, -6) {arrayrow={Paso 4 (\texttt{lista[9] >= 18}):}{18}{20}};
\pic {arraymark={19}{izq\,=\,der\,=\,medio\,=\,9}{0.25cm}};
\end{tikzpicture}
\end{center}
\caption{Ejemplo de una búsqueda usando el algoritmo de búsqueda binaria.
Como no se encontró al valor buscado, devuelve $-1$.}
\label{fig:busqbin}
\end{figure}

En el Código~\ref{busquedabinaria} mostramos una posible implementación de
este algoritmo, incluyendo una instrucción de depuración con |print| para
verificar su funcionamiento.

\begin{codigo}{busqueda\_binaria.py}{Función de búsqueda binaria}
\label{busquedabinaria}
\lstinputlisting{src/8_busqueda/busb.py}
\end{codigo}

A continuación varias ejecuciones de prueba:

\begin{codigo-python-sn}
>>> busqueda_binaria([1, 3, 5], 0)
[DEBUG] izq: 0 der: 2 medio: 1
[DEBUG] izq: 0 der: 0 medio: 0
-1
>>> busqueda_binaria([1, 3, 5], 1)
[DEBUG] izq: 0 der: 2 medio: 1
[DEBUG] izq: 0 der: 0 medio: 0
0
>>> busqueda_binaria([1, 3, 5], 2)
[DEBUG] izq: 0 der: 2 medio: 1
[DEBUG] izq: 0 der: 0 medio: 0
-1
>>> busqueda_binaria([1, 3, 5], 3)
[DEBUG] izq: 0 der: 2 medio: 1
1
>>> busqueda_binaria([1, 3, 5], 5)
[DEBUG] izq: 0 der: 2 medio: 1
[DEBUG] izq: 2 der: 2 medio: 2
2
>>> busqueda_binaria([1, 3, 5], 6)
[DEBUG] izq: 0 der: 2 medio: 1
[DEBUG] izq: 2 der: 2 medio: 2
-1
>>> busqueda_binaria([], 0)
-1
>>> busqueda_binaria([1], 1)
[DEBUG] izq: 0 der: 0 medio: 0
0
>>> busqueda_binaria([1], 3)
[DEBUG] izq: 0 der: 0 medio: 0
-1
\end{codigo-python-sn}

\ejercicioc{En la línea 13 de |busqueda_binaria.py| efectuamos la división usando el
operador |//| en lugar de |/|. ¿Qué sucedería si utilizáramos |/|?  }

\subsection*{¿Cuántas comparaciones hace este programa?}

Para responder esto pensemos en el peor caso, es decir, que se descartaron
varias veces partes del segmento para finalmente llegar a un segmento vacío y
el valor buscado no se encontraba en la lista.

En cada paso el segmento se divide por la mitad y se desecha una de esas
mitades, y en cada paso se hace una comparación con el valor buscado. Por lo
tanto, la cantidad de comparaciones que hacen con el valor buscado es
aproximadamente igual a la cantidad de pasos necesarios para llegar a un
segmento de tamaño 1.
Veamos el caso más sencillo para razonar, y supongamos que la longitud de la
lista es una potencia de 2, es decir \lstinline+len(lista)+~$= 2^k$:

\begin{itemize}
\item Luego del primer paso, el segmento a tratar es de tamaño $2^k$.
\item Luego del segundo paso, el segmento a tratar es de tamaño $2^{k-1}$.
\item Luego del tercer paso, el segmento a tratar es de tamaño $2^{k-2}$.

$\ldots$

\item Luego del paso $k$, el segmento a tratar es de tamaño $2^{k-k}=1$.
\end{itemize}

Por lo tanto este programa hace aproximadamente $k$ comparaciones con el valor
buscado cuando \lstinline+len(lista)+~$= 2^k$.
Pero si despejamos $k$ de la ecuación anterior, podemos ver que este programa
realiza aproximadamente $\log_2($\lstinline+len(lista)+$)$ comparaciones.

Cuando \lstinline+len(lista)+ no es una potencia de 2 el razonamiento es menos
prolijo, pero también vale que este programa realiza aproximadamente
$\log_2$(\lstinline+len(lista)+$)$ comparaciones. Concluimos entonces que:

\begin{observacion}
Si podemos suponer que la lista está previamente ordenada, podemos utilizar el
algoritmo de búsqueda binaria, cuyo comportamiento es proporcional al
\emph{logaritmo} de la cantidad de elementos de la lista, y por lo tanto
\emph{muchísimo} más eficiente que la búsqueda lineal.
\end{observacion}

Veamos un ejemplo para entender cuánto más eficiente es la búsqueda binaria.
Supongamos que tenemos una lista de un millón de elementos.

\begin{itemize}
\item El algoritmo de búsqueda lineal hará una cantidad de operaciones proporcional
a un millón; es decir que en el peor caso hará $1\,000\,000$ comparaciones, y en
un caso promedio, $500\,000$ comparaciones.
\item El algoritmo de búsqueda binaria hará como máximo $\log_2(1\,000\,000)$
comparaciones, o sea ¡no más que 20 comparaciones!.
\end{itemize}

\label{lookup-listas}
\begin{sabias_que}
    Para que el algoritmo de búsqueda binaria sea eficiente hay un requisito
    adicional, que nosotros dimos por sentado: si la lista |L| tiene $N$ elementos,
    el esfuerzo computacional para evaluar |L[i]| debe ser el mismo \emph{sin
    importar el valor de $N$ o de} |i|.

    Por ejemplo, si la lista tiene un millón de elementos, el esfuerzo
    computacional de evaluar |L[0]| debe ser el mismo que para evaluar
    |L[500000]| o |L[999999]|; y el mismo que si la lista tuviera 10 millones o
    100 millones.  Cuando esto ocurre, decimos que la operación |L[i]| es de
    \emph{tiempo constante}.

    La implementación interna de las tuplas, listas y cadenas en Python
    garantiza que se cumple esta condición.
\end{sabias_que}

\section{Resumen}

\begin{itemize}

\item La {\bf búsqueda} de un elemento en una secuencia es un
algoritmo básico pero importante. El problema que intenta resolver puede
plantearse de la siguiente manera: Dada una secuencia de valores y un
valor, devolver el índice del valor en la secuencia, si se encuentra, de no
encontrarse el valor en la secuencia señalizarlo apropiadamente.

\item Una de las formas de resolver el problema es mediante la {\bf
búsqueda lineal}, que consiste en ir revisando uno a uno los elementos de
la secuencia y comparándolos con el elemento a buscar.  Este algoritmo no
requiere que la secuencia se encuentre ordenada, la cantidad de comparaciones
que realiza es proporcional a |len(secuencia)|.

\item Cuando la secuencia sobre la que se quiere buscar está ordenada, se
puede utilizar el algoritmo de {\bf búsqueda binaria}.  Al estar ordenada
la secuencia, se puede desacartar en cada paso la mitad de los elementos,
quedando entonces con una eficiencia algorítmica proporcional a
$log_2($\lstinline!len(secuencia)!$)$.

\item El análisis del comportamiento de un algoritmo puede ser muy engañoso
si se tiene en cuenta el mejor caso, por eso suele ser mucho más
ilustrativo tener en cuenta el {\bf peor caso}.  En algunos casos
particulares podrá ser útil tener en cuenta, además, el {\bf caso
promedio}.
\end{itemize}


\newpage
\section{Ejercicios}

\extractionlabel{guia}
\begin{ejercicio}
Escribir una función que reciba una lista desordenada y un elemento, que:
\begin{partes}
\item Busque todos los elementos coincidan con el pasado por parámetro y
devuelva la cantidad de coincidencias encontradas.
\item Busque la primera coincidencia del elemento en la lista y devuelva su
posición.
\item Utilizando la función anterior, busque todos los elementos que coincidan
con el pasado por parámetro y devuelva una lista con las posiciones.
\end{partes}
\end{ejercicio}


\extractionlabel{guia}
\begin{ejercicio}
Escribir una función que reciba una lista de números no ordenada, que:
\begin{partes}
\item Devuelva el valor máximo.
\item Devuelva una tupla que incluya el valor máximo y su posición.
\item ¿Qué sucede si los elementos son cadenas de caracteres?
\end{partes}
{\bf Nota:} no utilizar \verb!lista.sort()!
\end{ejercicio}


\extractionlabel{guia}
\begin{ejercicio}
{\bf Agenda simplificada} \\
Escribir una función que reciba una cadena a buscar y una lista de tuplas
(nombre\_completo, telefono), y busque dentro de la lista, todas las
entradas que contengan en el nombre completo la cadena recibida (puede
ser el nombre, el apellido o sólo una parte de cualquiera de ellos).
Debe devolver una lista con todas las tuplas encontradas.
\end{ejercicio}


\extractionlabel{guia}
\begin{ejercicio}
{\bf Sistema de facturación simplificado} \\
Se cuenta con una lista ordenada de productos, en la que uno consiste en
una tupla de (identificador, descripción, precio), y una lista de los
productos a facturar, en la que cada uno consiste en una tupla de
(identificador, cantidad). \\
Se desea generar una factura que incluya la cantidad, la descripción, el
precio unitario y el precio total de cada producto comprado, y al final
imprima el total general. \\
Escribir una función que reciba ambas listas e imprima por
pantalla la factura solicitada.
\end{ejercicio}


\extractionlabel{guia}
\begin{ejercicio}
Escribir una función que reciba una lista ordenada y un elemento. Si el
elemento se encuentra en la lista, debe encontrar su posición mediante
búsqueda binaria y devolverlo.  Si no se encuentra, debe agregarlo a la
lista en la posición correcta y devolver esa nueva posición. (No utilizar
\verb!lista.sort()!.)
\end{ejercicio}


\newpage
\begin{subappendices}
\section{Filtros, transformaciones y acumulaciones}

Supongamos que manejamos una librería, y disponemos de una base de datos con el
inventario. Cada entrada del inventario está compuesta por el título del libro,
el autor, la cantidad disponible y el precio. Por ejemplo:

\begin{center}
\small
\rowcolors[]{2}{}{light-gray}
\begin{tabular}{p{8cm} p{3cm} r r}
{\bf Título} & {\bf Autor} & {\bf Cantidad} & {\bf Precio} \\
\hline
The Art of Computer Programming, Volumes 1-4 & Donald Knuth & 12 & 179.62 \\
Concrete Mathematics: A Foundation for Computer Science & Donald Knuth & 5 & 54.77 \\
The Pragmatic Programmer: From Journeyman to Master & Andrew Hunt, David Thomas & 3 & 33.17 \\
Clean Code: A Handbook of Agile Software Craftsmanship & Robert C. Martin & 7 & 38.99 \\
Code Complete: A Practical Handbook of Software Construction & Steve McConnell & 0 & 29.97 \\
Learning Python & Mark Lutz & 4 & 40.95 \\
\ldots & \ldots & \ldots & \ldots \\ \hline
\end{tabular}
\end{center}

Podemos representar nuestro inventario en Python utilizando una lista de
tuplas:

\begin{codigo-python-sn}
inventario = [
    ('The Art of Computer Programming, Volumes 1-4',
     'Donald Knuth', 12, 179.62),
    ('Concrete Mathematics: A Foundation for Computer Science',
     'Donald Knuth', 5, 54.77),
    ('The Pragmatic Programmer: From Journeyman to Master',
     'Andrew Hunt and David Thomas', 3, 33.17),
    ...
]
\end{codigo-python-sn}

Una vez que disponemos de nuestro inventario en una estructura de datos,
podemos sacar todo tipo de
reportes: la cantidad total de libros, el valor total del
inventario, el precio promedio por libro, etc.

Veamos un ejemplo simple: supongamos que queremos obtener la cantidad total de
libros de un autor determinado. Podemos diseñar un algoritmo muy simple:

\begin{codigo-python-sn}
def total_libros_autor(inventario, autor_buscado):
    total = 0
    for titulo, autor, cantidad, precio in inventario:
        if autor == autor_buscado:
            total += cantidad
    return total
\end{codigo-python-sn}

Otro ejemplo: queremos obtener la cantidad de títulos de los cuales no hay
suficiente stock (menos de 5 unidades):

\begin{codigo-python-sn}
def cantidad_poco_stock(inventario):
    total = 0
    for titulo, autor, cantidad, precio in inventario:
        if cantidad < 5:
            total += 1
    return total
\end{codigo-python-sn}

¿Y si quisiéramos obtener la lista de títulos cuyo precio supera los \$100?

\begin{codigo-python-sn}
def titulos_caros(inventario):
    titulos = []
    for titulo, autor, cantidad, precio in inventario:
        if precio > 100:
            titulos.append(titulo)
    return titulos
\end{codigo-python-sn}

Acabamos de \enquote{inventar} tres algoritmos para resolver tres problemas
diferentes\ldots\ pero ¿son realmente diferentes? ¿No tienen nada en común?

Si observamos con detenimiento, los tres algoritmos comparten un mismo esquema:

\begin{codigo-nohl-sn}
def f(L):
    inicializar acumulador
    por cada elemento en el la lista L:
        si se cumple alguna condición:
            hacer algún cálculo en base al elemento y acumular
    devolver acumulador
\end{codigo-nohl-sn}

Y este esquema en el fondo puede pensarse como una composición de tres
problemas más simples:

\begin{enumerate}
    \item A partir de una lista, \emph{filtrar} la lista según una condición
        determinada y obtener una lista con los elementos que pasan la
        condición.
    \item A partir de una lista, aplicar una \emph{transformación} a cada elemento
        y obtener una lista con los resultados.
    \item A partir de una lista, \emph{acumular} los elementos según un
        criterio determinado.
\end{enumerate}

% TODO: dibujo

Por ejemplo, podemos repensar nuestro primer algoritmo, que nos permitía calcular
la cantidad total de libros de un determinado autor, como:

\begin{enumerate}
    \item A partir del inventario, \emph{filtrar} según el autor:
        el resultado será una lista que contiene los libros del autor buscado.
    \item A partir de la lista obtenida, \emph{transformar} cada una de las tuplas
        |(titulo, autor, cantidad, precio)| para descartar todo menos la
        |cantidad|. Es decir, nos quedamos con una lista de números
        enteros, donde cada número representa una cantidad de libros.
    \item A partir de la lista obtenida, \emph{acumular} los elementos sumando uno
        a uno.
\end{enumerate}

Una ventaja de pensar el algoritmo de esta manera es que Python nos provee una
forma muy fácil de implementar filtros y transformaciones: las \emph{listas por
comprensión}.

\subsection{Listas por comprensión}

Concentrémonos en el filtro según el autor propuesto en el ejemplo anterior.
Una forma de implementarlo es:

\begin{codigo-python-sn}
def filtrar_autor(inventario, autor_buscado):
    filtrado = []
    for libro in inventario:
        if libro[2] == autor_buscado:
            filtrado.append(libro)
    return filtrado
\end{codigo-python-sn}

En Python podemos obtener el mismo resultado utilizando una \emph{lista por
comprensión}:

\begin{codigo-python-sn}
def filtrar_autor(inventario, autor_buscado):
    return (@[libro for libro in inventario if libro[2] == autor_buscado]@)
\end{codigo-python-sn}

Lo que vemos aquí es una sintaxis especial, que nos permite crear una lista
filtrando una secuencia según una condición:

\begin{codigo-python-sn}
(@[@)<variable> (@for@) <variable> (@in@) <secuencia> (@if@) <condicion>(@]@)
\end{codigo-python-sn}

Para aplicar la transformación propuesta (quedándonos únicamente con las
cantidades), podríamos implementarlo de esta manera:

\begin{codigo-python-sn}
def obtener_cantidades(inventario):
    cantidades = []
    for titulo, autor, cantidad, precio in inventario:
        cantidades.append(cantidad)
    return cantidades
\end{codigo-python-sn}

Pero en este caso también podemos obtener el mismo resultado con una lista por
comprensión:

\begin{codigo-python-sn}
def obtener_cantidades(inventario):
    return (@[cantidad for titulo, autor, cantidad, precio in inventario]@)
\end{codigo-python-sn}

En este caso la sintaxis utilizada es un poco diferente:

\begin{codigo-python-sn}
(@[@)<expresión> (@for@) <variable> (@in@) <secuencia>(@]@)
\end{codigo-python-sn}

Opcionalmente podemos combinar el filtro y la transformación en una única lista
por comprensión:

\begin{codigo-python-sn}
def cantidades_autor(inventario, autor_buscado):
    return [
        cantidad
        for titulo, autor, cantidad, precio in inventario
        if autor == autor_buscado
    ]
\end{codigo-python-sn}

\begin{observacion}
Es decir que la sintaxis general de las listas por comprensión es:
\begin{codigo-python-sn}
(@[@)<expresión> (@for@) <variable> (@in@) <secuencia> (@if@) <condicion>(@]@)
\end{codigo-python-sn}
\end{observacion}

Volviendo a nuestro problema inicial: obtener la cantidad total de libros del
autor; ya tenemos una forma de filtrar y transformar, y lo único que nos falta
es la acumulación. Pero recordemos que ya conocíamos una forma simple de
acumular sumando elementos: ¡la función |sum|!

\begin{codigo-python-sn}
def total_libros_autor(inventario, autor_buscado):
    return sum([
        cantidad
        for titulo, autor, cantidad, precio in inventario
        if autor == autor_buscado
    ])
\end{codigo-python-sn}

Planteemos ahora las soluciones para los otros dos problemas utilizando
filtros, transformaciones y acumulaciones. Nuestro segundo problema era obtener
la cantidad de títulos de los cuales no hay suficiente stock.

\begin{enumerate}
    \item Filtramos según la cantidad de stock, quedándonos con los libros
        para los cuales |cantidad < 5|.
    \item No es necesario aplicar una transformación.
    \item Solo necesitamos la cantidad de títulos, y eso es simplemente la
        cantidad de elementos de la lista producida en el paso anterior. Es
        decir que nuestra función de acumulación es |len|.
\end{enumerate}

\begin{codigo-python-sn}
def cantidad_poco_stock(inventario):
    return len([libro for libro in inventario if libro[3] < 5])
\end{codigo-python-sn}

Nuestro tercer problema era obtener la lista de títulos cuyo precio supera los
\$100.

\begin{enumerate}
    \item Filtramos según el precio, quedándonos con la lista de libros con
        |precio > 100|.
    \item Transformamos cada tupla quedándonos únicamente con el |titulo|.
    \item No es necesario aplicar una acumulación.
\end{enumerate}

\begin{codigo-python-sn}
def titulos_caros(inventario):
    return [
        titulo
        for titulo, autor, cantidad, precio in inventario
        if precio > 100
    ]
\end{codigo-python-sn}

Comparando estas soluciones con las primeras tres soluciones
propuestas, vemos que son dos estilos de programación diferentes:

\begin{itemize}
    \item Las primeras soluciones corresponden a un estilo más \emph{procedural} e
        \emph{imperativo}. Cuando pensamos en este estilo nos concentramos en
        dar órdenes para especificar \emph{cómo} queremos que la computadora
        resuelva el problema, paso por paso.
    \item Las soluciones planteadas utilizando filtros, transformaciones y
        acumulaciones corresponden a un estilo más \emph{funcional} y
        \emph{declarativo}, en el cual dividimos el problema en sub-problemas
        más simples, y nos concentramos en especificar cómo es el flujo de datos.
\end{itemize}

La discusión acerca de si uno de los dos estilos es \enquote{mejor} que el otro queda
fuera del alcance de este apunte, pero en general se considera que el uso de
listas por comprensión es \emph{idiomático} en Python. Es decir, los
programadores Python experimentados van a preferir leer y escribir código que
utilice listas por comprensión en lugar de implementar los filtros y
transformaciones a mano.

Por último, observamos que las listas por comprensión ofrecen una alternativa
sintáctica a las funciones |map| y |filter|, explicadas en el
Apéndice~\ref{map-filter}. Dejamos como ejercicio implementar las funciones
|total_libros_autor|, |cantidad_poco_stock| y |titulos_caros| en términos de
|map| y |filter|.
\end{subappendices}
