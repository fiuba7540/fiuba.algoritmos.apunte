\chapter{Manejo de archivos}
\label{uni:archivos}

Veremos en esta unidad cómo manipular archivos desde nuestros programas. Los
archivos permiten almacenar información que persistirá luego de que el programa
finalice su ejecución. Los archivos también se pueden compartir o ser
transmitidos entre diferentes computadoras, mediante dispositivos de
almacenamiento o redes como Internet.

\section{¿Qué es un archivo?}

Un archivo no es otra cosa más que una secuencia de bytes. Por ejemplo, un
archivo puede contener la secuencia de 4 bytes: |48 6f 6c 61| (notación
hexadecimal).

\begin{sabias_que}
\edef\myindent{\the\parindent}%
\noindent%
\begin{minipage}{.55\textwidth}
Un byte está formado de 8 bits, y según el valor de cada uno de los bits, un byte
puede representar $2^8 = 256$ combinaciones diferentes.
Si asignamos un valor numérico a cada una de esas combinaciones, comenzando
de 0, con un byte podemos representar cualquier número entre 0 y 255.

\setlength{\parindent}{\myindent}
Cuando se representan datos binarios, en lugar de utilizar la notación
binaria (base 2) o la decimal (base 10), se suele utilizar la notación
\emph{hexadecimal} (base 16).

En Python se puede escribir un número en notación binaria con el prefijo |0b|,
y en notación hexadecimal con el prefijo |0x|. Además, las funciones |bin|
y |hex| permiten obtener la representación de un número en binario y
hexadecimal, respectivamente.

\begin{codigo-python-sn}
>>> 0b11111101
253
>>> 0xfd
253
>>> bin(0xfd)
'0b11111101'
>>> hex(253)
'0xfd'
\end{codigo-python-sn}
\end{minipage}\hfill%
\begin{minipage}{.44\textwidth}
\begin{center}
\begin{tabular}{r r r}
{\bf Binario} & {\bf Decimal} & {\bf Hexadecimal} \\
\hline
\lstinline!00000000! & \lstinline!0! & \lstinline!00! \\
\lstinline!00000001! & \lstinline!1! & \lstinline!01! \\
\lstinline!00000010! & \lstinline!2! & \lstinline!02! \\
\lstinline!00000011! & \lstinline!3! & \lstinline!03! \\
\lstinline!00000100! & \lstinline!4! & \lstinline!04! \\
\lstinline!00000101! & \lstinline!5! & \lstinline!05! \\
\lstinline!00000110! & \lstinline!6! & \lstinline!06! \\
\lstinline!00000111! & \lstinline!7! & \lstinline!07! \\
\lstinline!00001000! & \lstinline!8! & \lstinline!08! \\
\lstinline!00001001! & \lstinline!9! & \lstinline!09! \\
\lstinline!00001010! & \lstinline!10! & \lstinline!0a! \\
\lstinline!00001011! & \lstinline!11! & \lstinline!0b! \\
\lstinline!00001100! & \lstinline!12! & \lstinline!0c! \\
\lstinline!00001101! & \lstinline!13! & \lstinline!0d! \\
\lstinline!00001110! & \lstinline!14! & \lstinline!0e! \\
\lstinline!00001111! & \lstinline!15! & \lstinline!0f! \\
\lstinline!00010000! & \lstinline!16! & \lstinline!10! \\
\lstinline!...!      & \lstinline!...! & \lstinline!...! \\
\lstinline!11111101! & \lstinline!253! & \lstinline!fd! \\
\lstinline!11111110! & \lstinline!254! & \lstinline!fe! \\
\lstinline!11111111! & \lstinline!255! & \lstinline!ff! \\
\end{tabular}
\end{center}
\end{minipage}
\end{sabias_que}

Un archivo se identifica con un \emph{nombre}, por ejemplo |hola.txt|. Para
facilitar la gestión y búsqueda eficiente, los archivos se organizan en
\emph{carpetas} y \emph{subcarpetas}, formando una estructura jerárquica: cada
carpeta puede contener archivos y otras carpetas, permitiendo una organización
lógica y estructurada de la información.

La ubicación de un archivo se identifica mediante una \emph{ruta}, que es una
cadena formada por la secuencia de carpetas y subcarpetas que lleva a dicho
archivo desde la \emph{carpeta raíz} (\lstinline!/!). Por ejemplo, si nuestro
archivo se encuentra dentro de la carpeta |home| y subcarpeta |alan|, su ruta
sería \verb!"/home/alan/hola.txt"!\footnote{En sistemas Windows se utiliza el
caracter {\tt \textbackslash} como separador en lugar del caracter
\lstinline!/!. Sin embargo, al especificar rutas en nuestros programas Python
se acepta que utilicemos el separador \lstinline!/!; de esta manera nuestros
programas podrán funcionar en cualquier sistema operativo.}. Una ruta se puede
escribir en forma \emph{absoluta} (comenzando con \lstinline!/! y conteniendo
la secuencia completa de carpetas y subcarpetas desde la carpeta raíz), o
\emph{relativa} a alguna carpeta (sin comenzar con \lstinline!/!). En nuestro
ejemplo, la ruta del archivo relativa a la carpeta \verb!"/home"! sería
\verb!"alan/hola.txt"!.

\section{Formatos de archivos}

Para cualquier información que se desee almacenar en un archivo, se debe elegir
una codificación que permita representar esa información mediante una secuencia
de bytes.

Por ejemplo, si deseamos almacenar el texto \texttt{"Hola mundo!"} en un archivo, lo
más simple sería elegir la codificación ASCII, en la que cada caracter se
almacena como 1 byte. De esta manera el archivo contendría en total 11 bytes:
|48 6f 6c 61 20 6d 75 6e 64 6f 21|.
Según la codificación ASCII, el caracter |H| se codifica con el valor
hexadecimal |48|, el caracter |o| con el valor |6f|, y así sucesivamente.

ASCII es un ejemplo de un \emph{formato de codificación de texto}, pero no es
el único. La limitación principal del formato ASCII es que solo permite
representar 128 caracteres. El formato UTF-8 es más complejo que ASCII,
ya que representa cada caracter con una cantidad variable de bytes, pero a
cambio permite representar más de un millón de caracteres. UTF-8 es la codificación
que se usa por defecto en la mayoría de los sistemas.

Si en lugar de texto deseamos almacenar una imagen en un archivo, tendríamos
que elegir otra codificación. En la Figura~\ref{fig-bmp} se muestra una imagen
codificada en formato BMP. Notar que en este caso el algoritmo de codificación
no es tan directo o intuitivo.

\begin{figure}[htb]
\hfill%
\begin{minipage}{.1\textwidth}
  \includegraphics[width=\textwidth]{graficos/bmp}
\end{minipage}%
\hfill%
\begin{minipage}{.85\textwidth}\raggedright%
\noindent{\footnotesize\tt%
4d 42 00 ba 00 00 00 00 00 00 00 8a 00 00 00 7c 00 00 00 04 00 00 00 04 00 00 \\
00 01 00 18 00 00 00 00 00 30 00 00 2e 23 00 00 2e 23 00 00 00 00 00 00 00 00 \\
00 00 00 00 00 ff ff 00 00 00 00 ff 00 00 00 00 00 00 47 42 73 52 00 00 00 00 \\
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 \\
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 02 00 00 00 00 00 00 \\
00 00 00 00 00 00 00 00 11 11 ff f9 00 00 11 11 ff f9 00 00 00 ff 11 00 f9 11 \\
00 ff 11 00 f9 11 11 11 ff f9 00 00 11 11 ff f9 00 00 00 ff 11 00 f9 11 00 ff \\
11 00 f9 11 \\}%
\end{minipage}
\hfill%
\caption{Una imagen de 4x4 pixels codificada en formato BMP.}
\label{fig-bmp}
\end{figure}

BMP, JPG y PNG son algunos ejemplos de formatos de codificación de imágenes.
También hay formatos de codificación para sonido, video y cualquier otro tipo
de información. Se dice que todos estos son \emph{formatos binarios}, en
contraste con los formatos de texto.\footnote{La distinción entre formatos
\enquote{de texto} y \enquote{binarios} es histórica, pero cabe destacar que técnicamente
todos los formatos son binarios, incluso los de texto.}

Notar que el archivo está definido únicamente por su contenido (la secuencia de
bytes), independientemente de lo que representen esos bytes. Si solo tenemos el
contenido de un archivo dado, no hay manera de saber qué codificación se
utilizó para crear esos datos; es decir, de qué formato es el archivo. Es por
eso que se utiliza la convención de agregar una \emph{extensión} al nombre del
archivo para indicar su formato. Por ejemplo, a los archivos de texto se les
agrega la extensión |.txt|, a los archivos en formatp BMP se les agrega la
extensión |.bmp|, etc. Si bien es una práctica recomendable, no es obligatorio
que el nombre del archivo incluya una extensión, o que concuerde con el formato
real del archivo.

A continuación veremos cómo manipular archivos en nuestros programas Python.

\section{Abrir un archivo}

En Python hay dos \emph{modos} para acceder a un archivo:

\begin{itemize}
    \item {\bf Modo texto} en el que podemos leer o escribir cadenas de texto (|str|), que
        serán codificadas automáticamente según la codificación elegida (ASCII, UTF-8, etc.).
        En la mayoría de los sistemas, el modo por defecto es texto con codificación UTF-8.
    \item {\bf Modo binario} en el que no se aplica ninguna codificación, y leemos o escribimos
        datos de tipo |bytes|.
\end{itemize}

Para poder leer o escribir un archivo, primero debemos pedirle permiso al
sistema operativo. Esta operación se llama \emph{abrir} el archivo. En Python,
para abrir un archivo usaremos la función \lstinline!open!, que recibe la
ruta del archivo a abrir.

\begin{codigo-python-sn}
archivo = open("archivo.txt")
\end{codigo-python-sn}

Esta función intentará abrir el archivo con el nombre indicado, por defecto en
modo texto. Si tiene éxito devolverá un valor de un tipo especial, que nos
permitirá manipular el archivo de diversas maneras.

\section{Leer un archivo de texto}

La operación más sencilla a realizar sobre un archivo es leer su contenido.
Podemos utilizar la función |read| para leer uno o más caracteres. Por ejemplo,
para almacenar en |s| una cadena con los 3 primeros caracteres leidos del archivo:

\begin{codigo-python-sn}
s = archivo.read(3)
\end{codigo-python-sn}

Si volvemos a invocar a |read(3)| se leerán los próximos 3 caracteres del archivo.
Esto es así ya que cada archivo que se encuentre abierto tiene una
posición asociada, que indica el último punto que fue leído.  Cada vez que
se lee un byte, avanza esa posición.

Lo más usual al trabajar con archivos de texto es procesarlos línea por línea;
es decir, leer hasta que encontramos el caracter \emph{nueva línea} o \verb!\n!. La
función |readline| hace esto de forma automática:

\begin{sabias_que}
Los archivos de texto son sencillos de manejar, pero existen por lo menos 3
formas distintas de marcar un fin de línea. En Unix tradicionalmente se usa
el caracter '\verb!\n!' (código ASCII~10, definido como \enquote{nueva línea}) para
el fin de línea, mientras que en los sitemas de Apple el fin de línea se solía
representar como un '\verb!\r!' (valor ASCII~13, definido como retorno de
carro) y en Windows se usan ambos caracteres '\verb!\r\n!'.

Al leer archivos en modo texto, Python acepta cualquier tipo de fin
de línea como si fuese un |\n|. Al escribir archivos, Python elegirá
automáticamente el modo más apropiado. Si queremos modificar este
comportamiento podemos especificar el modo utilizando la opción |newline| de la
función |open|.
\end{sabias_que}

\begin{codigo-python-sn}
linea = archivo.readline()
while linea != '':
    # procesar linea
    linea = archivo.readline()
\end{codigo-python-sn}

El lenguaje Python nos permite hacer lo mismo de una manera más abreviada:

\begin{codigo-python-sn}
for linea in archivo:
    # procesar linea
\end{codigo-python-sn}

De esta manera, la variable \lstinline!linea! irá almacenando distintas cadenas
correspondientes a cada una de las líneas del archivo.

Es posible, además, obtener todas las líneas del archivo utilizando una
sola llamada a función:

\begin{codigo-python-sn}
lineas = archivo.readlines()
\end{codigo-python-sn}

En este caso, la variable \lstinline!lineas! tendrá una lista de cadenas con
todas las líneas del archivo.

\begin{atencion}
Es importante tener en cuenta que cuando se utilizan funciones como
\lstinline!archivo.readlines()!, se está cargando en la memoria de la computadora
el contenido completo del archivo.  Siempre que una instrucción cargue un archivo
completo en memoria debe tenerse cuidado de utilizarla sólo con archivos
pequeños, ya que de otro modo podría agotarse la memoria de la computadora.
\end{atencion}

\section{Cerrar un archivo}

Al terminar de trabajar con un archivo, es importante cerrarlo,
por diversos motivos: en algunos sistemas los archivos sólo pueden
ser abiertos de a un programa por la vez; en otros, lo que se haya
escrito no se guardará realmente hasta no cerrar el archivo; o el
límite de cantidad de archivos que puede manejar un programa puede
ser bajo, etc.

Para cerrar un archivo simplemente se debe llamar a:

\begin{codigo-python-sn}
archivo.close()
\end{codigo-python-sn}

Además, Python nos provee con una estructura que permite cerrar el archivo
automáticamente, sin necesidad de llamar a |close|:

\begin{codigo-python-sn}
with open("archivo.txt") as archivo:
    #
    # procesar el archivo
    #

# Cuando la ejecución sale del bloque 'with',
# el archivo se cierra automáticamente.
\end{codigo-python-sn}

\section{Ejemplo: procesamiento de archivos de texto}

Por ejemplo, para mostrar todas las líneas de un archivo,
precedidas por el número de línea, podemos hacerlo de la siguiente manera:

\begin{codigo-python-sn}
archivo = open("archivo.txt")
i = 1
for linea in archivo:
    linea = linea.rstrip("\n")
    print(f"{i}: {linea}")
    i += 1
archivo.close()
\end{codigo-python-sn}

La llamada a \lstinline!rstrip! es necesaria ya que cada línea que se lee del
archivo contiene un caracter especial llamado \emph{fin de línea} y con la llamada a
\lstinline!rstrip("\n")! se remueve.

Notar que sería equivalente usar el bloque |with| para ahorrarnos la llamada a
|close|:

\begin{codigo-python-sn}
(@with open("archivo.txt") as archivo:@)
    i = 1
    for linea in archivo:
        linea = linea.rstrip("\n")
        print(f"{i}: {linea}")
        i += 1
\end{codigo-python-sn}

También podemos utilizar la función |enumerate| (explicada en la
sección~\ref{enumerate}) para no tener que mantener el
contador |i| a mano:

\begin{codigo-python-sn}
with open("archivo.txt") as archivo:
    (@for i, linea in enumerate(archivo):@)
        linea = linea.rstrip("\n")
        print(f"{i}: {linea}")
\end{codigo-python-sn}

\section{Modo de apertura de los archivos}

La función \lstinline!open! recibe un parámetro opcional para indicar el
modo en que se abrirá el archivo. Como ya mencionamos, un archivo se puede abrir en \textbf{modo texto} (|'t'|) o \textbf{modo binario} (|'b'|).

También se pueden especificar tres modos en cuanto a los permisos de lectura y escritura:

\begin{itemize}
\item Modo de \textbf{sólo lectura} (\lstinline!'r'!).   En este caso no es
posible realizar modificaciones sobre el archivo, solamente leer su
contenido.

\item Modo de \textbf{sólo escritura} (\lstinline!'w'!). En este caso el
archivo es truncado (vaciado) si existe, y se lo crea si no existe.

\item Modo \textbf{sólo escritura posicionándose al final del archivo}
(\lstinline!a!). En este caso se crea el archivo, si no existe, pero en
caso de que exista se posiciona al final, manteniendo el contenido
original.
\end{itemize}

Por ejemplo, para abrir un archivo en modo binario y escritura:

\begin{codigo-python-sn}
archivo = open("imagen.jpg", "wb")
\end{codigo-python-sn}

En cualquiera de los modos |r|, |w| o |a| se puede agregar un
\lstinline!+! para pasar a un modo lectura-escritura. El comportamiento de
\lstinline!r+! y de \lstinline!w+! no es el mismo, ya que en el primer caso
se tiene el archivo completo, y en el segundo caso se trunca el archivo,
perdiendo así los datos.

\begin{observacion}
Si un archivo no existe y se lo intenta abrir en modo lectura, se generará
un error; en cambio si se lo abre para escritura, Python se encargará de
crear el archivo al momento de abrirlo, ya sea con \lstinline!'w'!,
\lstinline!'a'!, \lstinline!'w+'! o con \lstinline!'a+')!.
\end{observacion}

En caso de que no se especifique el modo, los archivos serán abiertos en
modo sólo lectura (\lstinline!r!).

\begin{atencion}
Si un archivo existente se abre en modo escritura (\lstinline!'w'! o
\lstinline!'w+'!), todos los datos anteriores son borrados y reemplazados
por lo que se escriba en él.
\end{atencion}

\section{Escribir un archivo de texto}

De la misma forma que para la lectura, existen dos formas distintas de
escribir a un archivo.  Mediante cadenas:

\begin{codigo-python-sn}
archivo.write(cadena)
\end{codigo-python-sn}

O mediante listas de cadenas:

\begin{codigo-python-sn}
archivo.writelines(lista_de_cadenas)
\end{codigo-python-sn}

Así como la función \lstinline!readline! devuelve las líneas con los caracteres
de fin de línea (\lstinline!\n!), será necesario agregar los caracteres de
fin de línea a las cadenas que se vayan a escribir en el archivo.

Por ejemplo, el siguiente programa genera a su vez el código de otro programa
Python y lo guarda en el archivo |saludo.py|:

\begin{codigo-python-sn}
with open("saludo.py", "w") as saludo:
    saludo.write("# Este programa fue generado por otro programa!\n")
    saludo.write("print('Hola Mundo')\n")
\end{codigo-python-sn}

\begin{atencion}
Si un archivo existente se abre en modo lectura-escritura, al escribir en
él se sobreescribirán los datos anteriores, a menos que se haya llegado al
final del archivo.

Este proceso de sobreescritura se realiza caracter por caracter, sin
consideraciones adicionales para los caracteres de fin de línea ni otros
caracteres especiales.
\end{atencion}

\section{Agregar información a un archivo}

Abrir un archivo en modo \emph{agregar al final} puede parece raro,
pero es bastante útil.

Uno de sus usos es para escribir un archivo de bitácora (o archivo de
{\textit log}), que nos permita ver los distintos eventos que se fueron
sucediendo, y así encontrar la secuencia de pasos (no siempre evidente) que
hace nuestro programa.

Esta es una forma muy habitual de buscar problemas o hacer un seguimiento
de los sucesos. Para los administradores de sistemas es una herramienta
esencial de trabajo.

En el Código~\ref{modulo_log} se muestra un módulo para manejo de logs, que
se encarga de la apertura del archivo, del guardado de las líneas una por
una y del cerrado final del archivo.

\begin{codigo}{log.py}{Módulo para manipulación de archivos de log}
\label{modulo_log}
\lstinputlisting{src/11_archivos/log.py}
\end{codigo}

En este módulo se utiliza el módulo de Python \lstinline!datetime! para
obtener la fecha y hora actual que se guardará en los archivos.  Es
importante notar que en el módulo mostrado no se abre o cierra un archivo
en particular, sino que las funciones están programadas de modo tal que
puedan ser utilizadas desde otro programa.

Se trata de un módulo genérico que podrá ser utilizado por diversos programas,
que requieran la funcionalidad de registrar los posibles errores o eventos que
se produzcan durante la ejecución.

Para utilizar este módulo, será necesario primero llamar a
\lstinline!log.abrir()! para abrir el archivo de log, luego llamar a
\lstinline!log.guardar()! por cada mensaje que se quiera registrar, y
finalmente llamar a \lstinline!log.cerrar()! cuando se quiera concluir la
registración de mensajes:

\begin{codigo-python-sn}
import log

archivo_log = log.abrir("log.txt")
# ...
# Hacer cosas que pueden dar error
if error:
    log.guardar(archivo_log, "ERROR: " + error)
# ...
# Finalmente cerrar el archivo
log.cerrar(archivo_log)
\end{codigo-python-sn}

Este código, que incluye el módulo \lstinline!log! mostrado anteriormente,
muestra una forma básica de utilizar un archivo de log.  Al iniciarse el
programa se abre el archivo de log, de forma que queda registrada la fecha
y hora de inicio.  Posteriormente se realizan tareas varias que podrían
provocar errores, y de haber algún error se lo guarda en el archivo de log.
Finalmente, al terminar el programa, se cierra el archivo de log, quedando
registrada la fecha y hora de finalización.

El archivo de log generado tendrá la forma:

\begin{codigo-nohl-sn}
2016-04-10 15:20:32.229556 Iniciando registro de errores
2016-04-10 15:20:50.721415 ERROR: no se pudo acceder al recurso
2016-04-10 15:21:58.625432 ERROR: formato de entrada inválido
2016-04-10 15:22:10.109376 Fin del registro de errores
\end{codigo-nohl-sn}

\section{Archivos binarios}

Para abrir un archivo y manejarlo de forma binaria es necesario agregarle
una \verb!'b'! al parametro de modo:

\begin{codigo-python-sn}
archivo_binario = open('imagen.jpg', 'rb')
\end{codigo-python-sn}

Para procesar el archivo de a bytes en lugar de líneas, se utiliza la
función \lstinline!contenido = archivo.read(n)! para leer \lstinline!n!
bytes y \lstinline!archivo.write(contenido)!, para
escribir \lstinline!contenido! en la posición actual del archivo.

Al manejar un archivo binario, frecuentemente es necesario conocer la
posición actual en el archivo y poder modificarla. Para obtener la
posición actual se utiliza \lstinline!archivo.tell()!, que
indica la cantidad de bytes desde el comienzo del archivo.

Para modificar la posición actual se utiliza
\lstinline!archivo.seek(posicion)!, que permite desplazarse hacia el byte
ubicado en la posición indicada.

\begin{codigo-python-sn}
>>> f = open('imagen.jpg', 'rb')
>>> f.tell()
0
>>> datos = f.read(3)
>>> datos
b'\xff\xd8\xff'
>>> type(datos)
<class 'bytes'>
>>> f.tell()
3
>>> f.seek(0)
0
>>> datos = f.read() # leer el contenido completo del archivo
>>> len(datos)
3150
\end{codigo-python-sn}

\begin{atencion}
Al trabajar con archivos binarios, la función |read| no devuelve cadenas de
caracteres (|str|), sino que devuelve una \emph{secuencia de bytes} (|bytes|).
Análogamente, la función |write| recibe una secuencia de bytes.
\end{atencion}

\section{Persistencia de datos}

Se llama {\bf persistencia} a la capacidad de guardar la
información de un programa para poder volver a utilizarla en otro
momento. Es lo que los usuarios conocen como \emph{Guardar el archivo}
y después \emph{Abrir el archivo}. Pero para un programador puede
significar más cosas y suele involucrar un proceso de
\emph{serialización} de los datos a un archivo o a una base de datos o a
algún otro medio similar, y el proceso inverso de recuperar los
datos a partir de la información \emph{serializada}.

% Ejemplo Highscores

Por ejemplo, supongamos que en el desarrollo de un juego se quiere guardar
en un archivo la información referente a los ganadores, el puntaje máximo
obtenido y el tiempo de juego en el que obtuvieron ese puntaje.

En el juego, esa información podría estar almacenada en una lista de
tuplas:
\begin{codigo-python-sn}
[(nombre1, puntaje1, tiempo1), (nombre2, puntaje2, tiempo2), ...]
\end{codigo-python-sn}

Esta información se puede guardar en un archivo de muchas formas distintas.
En este caso, para facilitar la lectura del archivo de puntajes para los
humanos, se decide guardarlos en un archivo de texto, donde cada tupla
ocupará una línea y los valores de las tuplas estarán separados por
comas.

En el Código~\ref{puntajes} se muestra un módulo capaz de guardar y
recuperar los puntajes en el formato especificado.

\begin{codigo}{puntajes.py}{Módulo para guardar y recuperar puntajes en un archivo}
\label{puntajes}
\lstinputlisting{src/11_archivos/puntajes.py}
\end{codigo}

Dadas las especificaciones del problema al guardar los valores en el
archivo, es necesario convertir el puntaje (que es un valor numérico) en
una cadena, y al abrir el archivo es necesario convertirlo nuevamente en un
valor numérico.

\begin{observacion}
Es importante notar que tanto la función que almacena los datos como la que
los recupera requieren que la información se encuentre de una forma
determinada y de no ser así, fallarán.  Es por eso que estas condiciones se
indican en la documentación de las funciones como sus precondiciones. En
próximas unidades veremos cómo evitar que falle una función si alguna de
sus condiciones no se cumple.
\end{observacion}

Es bastate sencillo probar el módulo programado y ver que lo que se guarda
es igual que lo que se recupera:

\begin{codigo-python-sn}
>>> import puntajes
>>> valores = [("Pepe", 108, "4:16"), ("Juana", 2315, "8:42")]
>>> puntajes.guardar_puntajes("puntajes.txt", valores)
>>> recuperado = puntajes.recuperar_puntajes("puntajes.txt")
>>> print(recuperado)
[('Pepe', 108, '4:16'), ('Juana', 2315, '8:42')]
\end{codigo-python-sn}

% Fin ejemplo.

Guardar el estado de un programa se puede hacer tanto en un
archivo de texto, como en un archivo binario. En muchas
situaciones es preferible guardar la información en un archivo de
texto, ya que de esta manera es posible modificarlo fácilmente
desde cualquier editor de textos.

En general, los archivos de texto consumen más
espacio, pero son más faciles de entender y fáciles de usar desde
cualquier programa.

Por otro lado, en un archivo binario bien definido se puede evitar el
desperdicio de espacio, o también hacer que sea más eficiente acceder a los
datos.

En definitiva, la decisión de qué formato usar queda a discreción del
programador. Es importante recordar que el sentido común es el valor más
preciado en un programador.

\subsection{Persistencia en archivos CSV}

Un formato que suele usarse para transferir datos entre programas es
\textbf{CSV} (del inglés \emph{comma separated values}: valores separados
por comas). Es un formato bastante sencillo, tanto para leerlo como para
procesarlo desde el código, parecido al formato visto en el ejemplo
anteriormente.

\begin{codigo-nohl-sn}
Nombre,Apellido,Telefono,Cumpleaños
"John","Smith","555-0101","1973-11-24"
"Jane","Smith","555-0101","1975-06-12"
\end{codigo-nohl-sn}

En el ejemplo se puede ver una pequeña base de datos. En la primera línea
del archivo tenemos los nombres de los campos, un dato opcional desde el
punto de vista del procesamiento de la información, pero que facilita
entender el archivo.

En las siguientes lineas se ingresan los datos de la base de datos, cada
campo separado por comas. Los campos que son cadenas se suelen escribir
entre comillas dobles. Si alguna cadena contiene alguna comilla doble se la
reemplaza por \verb!\"! y una contrabarra se escribe como \verb!\\!.

En Python es bastante sencillo procesar de este tipo de archivos, tanto
para la lectura como para la escritura, mediante el módulo |csv|.

La funciones del ejemplo anterior podrían programarse mediante el módulo
|csv|.  En el Código~\ref{puntajes_csv} se muestra una posible implementación
que utiliza este módulo.  Si se prueba este código, se obtiene un resultado
idéntico al obtenido anteriormente.

\begin{codigo}{puntajes\_csv.py}{Módulo para guardar y recuperar puntajes en un archivo CSV}
\label{puntajes_csv}
\lstinputlisting{src/11_archivos/puntajes_csv.py}
\end{codigo}

El código en este caso es muy similar, ya que en el ejemplo original se
hacían muy pocas consideraciones al respecto de los valores: se asumía que
el primero y el tercero eran cadenas mientras que el segundo necesitaba ser
convertido a cadena.

\begin{observacion}
Es importante notar, entonces, que al utilizar el módulo \lstinline!csv!
en lugar de hacer el procesamiento en forma manual, se obtiene un
comportamiento más robusto, ya que el módulo \lstinline!csv! tiene en
cuenta muchos más casos que nuestro código original no. Por ejemplo, el
código anterior no tenía en cuenta que el nombre pudiera contener una coma.
\end{observacion}

En el apéndice de esta unidad puede verse una aplicación completa de una
agenda, que almacena los datos del programa en archivos CSV.

\subsection{Persistencia en archivos binarios}

En el caso de que decidiéramos grabar los datos en un archivo binario,
Python incluye varios módulos que pueden ser de ayuda. Entre ellos se
destacan los módulos |pickle| y |struct|.

El módulo |pickle| permite codificar automáticamente un valor de cualquier tipo
a una secuencia de bytes, y luego decodificarlo. Hay que tener en cuenta, sin
embargo, que no es nada simple acceder a un archivo en el formato |pickle|
desde un programa que no esté escrito en Python.

El módulo |struct| permite codificar y decodificar a mano cada fragmento de
información. Es bastante más complicado de usar que |pickle|, pero es esencial
si deseamos codificar datos binarios de forma tal que podamos decodificarlos en
otros sistemas o lenguajes.

En el Código~\ref{puntajes_pickle} se muestra el mismo ejemplo de
almacenamiento de puntajes, utilizando el módulo \lstinline!pickle!.

\begin{codigo}{puntajes\_pickle.py}{Módulo para guardar y recuperar puntajes en
    un archivo que usa \emph{pickle}}
\label{puntajes_pickle}
\lstinputlisting{src/11_archivos/puntajes_pickle.py}
\end{codigo}

En el apéndice de esta unidad puede verse una aplicación completa de una
agenda, que utiliza el módulo |struct| para almacenar datos en archivos.

\section{Resumen}

\begin{itemize}
\item Para utilizar un archivo desde un programa, es necesario abrirlo, y
cuando ya no se lo necesite, se lo debe cerrar.
\item Las instrucciones más básicas para manejar un archivo son leer y escribir.
\item Cada archivo abierto tiene relacionada una posición que se puede
consultar o cambiar.
\item Los archivos de texto se procesan generalmente línea por línea y
sirven para intercambiar información entre diversos programas o entre
programas y humanos.
\item Los archivos binarios se procesan generalmente byte por byte. Suelen ser
más eficientes al ser interpretados por una computadora, pero son ilegibles
para humanos.
\item Es posible acceder de forma secuencial a los datos, o se puede ir
accediendo a posiciones en distintas partes del archivo, dependiendo de
cómo esté almacenada la información y qué se quiera hacer con ella.
\item Es posible leer todo el contenido de un archivo y almacentarlo en una
única variable, pero si el archivo es muy grande puede consumir memoria
innecesariamente.
\end{itemize}

\begin{referencia_python}

\begin{sintaxis}{\lstinline{archivo = open(nombre[, modo])}}

Abre un archivo. |nombre| es el nombre completo del archivo,
|modo| especifica si se va usar para lectura ('\verb!r!'), escritura
truncando el archivo ('\verb!w!'), o escritura agregando al final del archivo
('\verb!a!'), agregándole un '\verb!+!' al modo el archivo se abre en
lectura-escritura, agregándole una '\verb!b!' el archivo se maneja como archivo
binario, agregándole '\verb!U!' los fin de línea se manejan a mano.
\end{sintaxis}

\begin{sintaxis}{\lstinline!archivo.close()!}
Cierra el archivo.
\end{sintaxis}

\begin{sintaxis}{\lstinline!with open(nombre) as archivo:!}
Abre el archivo para procesar dentro del bloque |with|. El archivo se
cerrará automáticamente al salir del bloque.
\end{sintaxis}

\begin{sintaxis}{\lstinline!linea = archivo.readline()!}
Lee una línea de texto del archivo

Si la cadena devuelta es vacía, es que se ha llegado al
final del archivo.
\end{sintaxis}

\begin{sintaxis}{\lstinline!for linea in archivo:!}
Itera sobre las lineas del archivo.
\end{sintaxis}

\begin{sintaxis}{\lstinline!lineas = archivo.readlines()!}
Devuelve una lista con todas las líneas del archivo.
\end{sintaxis}

\begin{sintaxis}{\lstinline!datos = archivo.read([n])!}
Si se trata de un archivo de texto, devuelve la cadena de |n|
caracteres situada en la posición actual del archivo.

Si se trata de un archivo binario, devuelve una secuencia de |n| bytes.

Si la secuencia devuelta es vacía, es que se ha llegado al
final del archivo.

De omitirse el parámetro \lstinline!n!, devuelve todo el contenido del archivo.
\end{sintaxis}

\begin{sintaxis}{\lstinline!archivo.write(contenido)!}
Escribe \lstinline!contenido! en la posición actual de \lstinline!archivo!.
\end{sintaxis}

\begin{sintaxis}{\lstinline!posicion = archivo.tell()!}
Devuelve un número que indica la posición actual en \lstinline!archivo!,
equivalente a la cantidad de bytes desde el comienzo del archivo.
\end{sintaxis}

\begin{sintaxis}{\lstinline!archivo.seek(posicion)!}
Modifica la posición actual en \lstinline!archivo!, trasladándose
hasta el byte |posicion|.
\end{sintaxis}

\begin{sintaxis}{\lstinline!os.path.exists(ruta)!}
Indica si la ruta existe o no.
No nos dice si es una carpeta, un archivo u otro tipo de archivo especial
del sistema.
\end{sintaxis}

\begin{sintaxis}{\lstinline!os.path.isfile(ruta)!}
Indica si la ruta existe y es un archivo.
\end{sintaxis}

\begin{sintaxis}{\lstinline!os.path.isdir(ruta)!}
Indica si la ruta existe y es una carpeta (directorio).
\end{sintaxis}

\begin{sintaxis}{\lstinline!os.path.join(ruta, ruta1[, ... rutaN]])!}
Une las rutas con el caracter de separación de carpetas que le corresponda
al sistema en uso.
\end{sintaxis}

\end{referencia_python}

\newpage
\section{Ejercicios}

\extractionlabel{guia}
\begin{ejercicio}
Escribir una función, llamada \lstinline|head| que reciba un archivo y un número
\lstinline!N! e imprima las primeras \lstinline!N! líneas del archivo.
\end{ejercicio}

\extractionlabel{guia}
\begin{ejercicio}
Escribir una función, llamada \lstinline|cp|, que copie todo el contenido de un
archivo (sea de texto o binario) a otro, de modo que quede exactamente igual.\\
{\bf Nota}: Tener en cuenta que el contenido completo del archivo puede ser más
grande que la memoria de la computadora. Utilizar
\lstinline!archivo.read(bytes)! para leer como máximo una cantidad
determinada de bytes.
\end{ejercicio}

\extractionlabel{guia}
\begin{ejercicio}
Escribir una función, llamada \lstinline|wc|, que dado un archivo de texto, lo procese e
imprima por pantalla cuántas líneas, cuantas palabras y cuántos caracteres
contiene el archivo.
\end{ejercicio}

\extractionlabel{guia}
\begin{ejercicio}
Escribir una función, llamada \lstinline|grep|, que reciba una cadena y un
archivo de texto, e imprima las líneas del archivo que contienen la cadena
recibida.
\end{ejercicio}

\extractionlabel{guia}
\begin{ejercicio}
Escribir una función, llamada \lstinline|rot13|, que reciba un archivo de texto de
origen y uno de destino, de modo que para cada línea del archivo origen, se
guarde una línea \emph{cifrada} en el archivo destino.  El algoritmo de cifrado
a utilizar será muy sencillo: a cada caracter comprendido entre la a y la z, se
le suma 13 y luego se aplica el módulo 26, para obtener un nuevo caracter.
\end{ejercicio}

\extractionlabel{guia}
\begin{ejercicio}
Sea una lista de números enteros entre -2.147.483.648 y 2.147.483.647.
\begin{partes}
  \item Escribir una función \lstinline!guardar_numeros! que reciba la lista
    y una ruta, y guarde el contenido de la lista en el archivo,
    en modo texto, escribiendo un número por línea.
  \item Escribir una función \lstinline!cargar_numeros! que reciba una ruta
    a un archivo con el formato anterior y devuelva la lista de números cargada.
  \item Modificar las funciones anteriores para que almacenen el archivo en formato
    binario, almacenando cada número en 4 bytes. Analizar las ventajas y
    desventajas entre el formato de texto y binario.
\end{partes}
\end{ejercicio}

\extractionlabel{guia}
\begin{ejercicio}
Sea un diccionario cuyas claves y valores son cadenas.
\begin{partes}
  \item Escribir una función \lstinline!guardar_diccionario! que reciba un diccionario
    y una ruta, y guarde el contenido del diccionario en el archivo, en modo texto,
    escribiendo un par clave-valor por línea con el formato \lstinline!clave,valor!.
  \item Escribir una función \lstinline!cargar_diccionario! que reciba una ruta a un
    archivo con el formato anterior y devuelva el diccionario cargado.
\end{partes}
\end{ejercicio}

\extractionlabel{guia}
\begin{ejercicio}
Sea una imagen de 8x8 pixels, en el que cada pixel puede ser blanco o negro.
La imagen se representa en Python como una matriz de 8x8 valores |bool|, donde un
valor |True| o |False| representa un pixel blanco o negro respectivamente.

\begin{partes}
  \item Escribir funciones para leer y escribir una imagen en un formato de texto.
    En este formato, el archivo contiene 8 líneas de 8 caracteres, y cada caracter
    representa un pixel. Un pixel blanco o negro se representa con un caracter ASCII
    |B| o |N| respectivamente.
  \item $\dificil$ Escribir funciones para leer y escribir una imagen en un formato binario.
    En este formato, la imagen se almacena en un archivo que contiene
    exactamente 64 bits (8 bytes), donde cada bit representa un pixel, y un pixel blanco
    o negro se representa con un bit 1 o 0, respectivamente.
\end{partes}
\end{ejercicio}

\newpage
\begin{subappendices}
\section{Agenda con archivos CSV}

A continuación se muestra un programa de agenda que utiliza archivos
CSV\@. Luego se muestran los cambios necesarios para que la agenda que utilice archivos
en formato binario utilizando el módulo |struct|, en lugar de CSV.

\tituloCodigo{\lstinline|agenda-csv.py| Agenda con los datos en formato CSV}
\lstinputlisting{src/11_archivos/agenda-csv.py}

\section{Agenda con archivos binarios}

\tituloCodigo{\lstinline|agenda-struct.py| Modificaciones a la agenda para
guardar los datos en formato binario, utilizando el módulo \texttt{struct}}
\lstinputlisting[firstline=1,lastline=49]{src/11_archivos/agenda-struct.py}

\end{subappendices}
