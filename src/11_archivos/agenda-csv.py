import csv, os, datetime

RUTA = "agenda.csv"
FORMATO_FECHA = "%d/%m/%Y"

def cargar_agenda(ruta):
    """Carga todos los datos del archivo en una lista y la devuelve."""
    agenda = []
    if not os.path.exists(ruta):
        return agenda
    with open(ruta) as f:
        datos_csv = csv.reader(f)
        encabezado = next(datos_csv)
        for item in datos_csv:
            nombre, apellido, telefono, nacimiento = item
            agenda.append((nombre, apellido, telefono, cadena_a_fecha(nacimiento)))
    return agenda

def guardar_agenda(agenda, ruta):
    """Guarda la agenda en el archivo."""
    with open(ruta, "w") as f:
        datos_csv = csv.writer(f)
        # cabecera:
        datos_csv.writerow(("Nombre", "Apellido", "Telefono", "FechaNacimiento"))
        for item in agenda:
            nombre, apellido, telefono, nacimiento = item
            datos_csv.writerow((nombre, apellido, telefono, fecha_a_cadena(nacimiento)))

def leer_busqueda():
    """Solicita al usuario nombre y apellido y los devuelve."""
    nombre = input("Nombre: ")
    apellido = input("Apellido: ")
    return nombre, apellido

def buscar(nombre, apellido, agenda):
    """Busca el primer item que coincida con nombre y con apellido."""
    for item in agenda:
        if nombre in item[0] and apellido in item[1]:
            return item
    return None

def menu_alta(nombre, apellido, agenda):
    """Pregunta si se desea ingresar un nombre y apellido y
       de ser así, pide los datos al usuario."""
    print(f"No se encuentra {nombre} {apellido} en la agenda.")
    confirmacion = input("¿Desea ingresarlo? (s/n): ")
    if confirmacion.lower() != "s":
        return
    telefono = input("Telefono: ")
    nacimiento = input("Fecha de nacimiento (dd/mm/aaaa): ")
    agenda.append([nombre, apellido, telefono, cadena_a_fecha(nacimiento)])

def mostrar_item(item):
    """Muestra por pantalla un item en particular."""
    nombre, apellido, telefono, nacimiento = item
    print()
    print(f"{nombre} {apellido}")
    print(f"Telefono: {telefono}")
    print(f"Fecha de nacimiento (dd/mm/aaaa): {fecha_a_cadena(nacimiento)}")
    print()

def menu_item():
    """Muestra por pantalla las opciones disponibles para un item
       existente."""
    o = input("b: borrar, m: modificar, ENTER para continuar (b/m): ")
    return o.lower()

def modificar(viejo, nuevo, agenda):
    """Reemplaza el item viejo con el nuevo, en la lista datos."""
    indice = agenda.index(viejo)
    agenda[indice] = nuevo

def menu_modificacion(item, agenda):
    """Solicita al usuario los datos para modificar una entrada."""
    nombre = input("Nuevo nombre: ")
    apellido = input("Nuevo apellido: ")
    telefono = input("Nuevo teléfono: ")
    nacimiento = input("Nueva fecha de nacimiento (dd/mm/aaaa): ")
    modificar(item, [nombre, apellido, telefono, cadena_a_fecha(nacimiento)], agenda)

def baja(item, agenda):
    """Elimina un item de la lista."""
    agenda.remove(item)

def confirmar_salida():
    """Solicita confirmación para salir"""
    confirmacion = input("¿Desea salir? (s/n): ")
    return confirmacion.lower() == "s"

def agenda():
    """Función principal de la agenda.
    Carga los datos del archivo, permite hacer búsquedas, modificar
    borrar, y al salir guarda. """
    agenda = cargar_agenda(RUTA)
    while True:
        nombre, apellido = leer_busqueda()
        if nombre + apellido == "":
            if confirmar_salida():
                break
        item = buscar(nombre, apellido, agenda)
        if not item:
            menu_alta(nombre, apellido, agenda)
            continue
        mostrar_item(item)
        opcion = menu_item()
        if opcion == "m":
            menu_modificacion(item, agenda)
        elif opcion == "b":
            baja(item, agenda)
    guardar_agenda(agenda, RUTA)

def fecha_a_cadena(fecha):
    """Convierte una fecha de tipo `date` a una cadena"""
    return fecha.strftime(FORMATO_FECHA)

def cadena_a_fecha(s):
    """Convierte una cadena a una fecha de tipo `date`"""
    return datetime.datetime.strptime(s, FORMATO_FECHA).date()

agenda()
