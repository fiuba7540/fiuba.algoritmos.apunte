\chapter[Funciones]{Funciones}
\label{chapter:funciones}

En la primera unidad vimos que el programador puede definir nuevas
instrucciones, que llamamos \emph{funciones}. En particular lo aplicamos a la
construcción de una función llamada \lstinline+hola+ que saluda a todos a
quienes queramos saludar:

\begin{codigo-python-sn}
def hola(alguien):
	return "Hola " + alguien + "! Estoy programando en Python."
\end{codigo-python-sn}

La función |hola| recibe un único \emph{parámetro} (|alguien|). Para llamar a
una función debemos asociar cada uno de los parámetros con algún valor
determinado (que se denomina \emph{argumento}). Por ejemplo, podemos invocar a
la función |hola| dos veces, para saludar a Ana y a Juan, haciendo que
\lstinline+alguien+ se asocie al valor \lstinline!"Ana"! en la primera llamada
y al valor \lstinline!"Juan"! en la segunda. La función en cada caso devolverá
un \emph{resultado} que que se calcula a partir del argumento.

\begin{codigo-python-sn}
>>> hola("Ana")
'Hola Ana! Estoy programando en Python.'
>>> hola("Juan")
'Hola Juan! Estoy programando en Python.'
\end{codigo-python-sn}

En general, las funciones pueden recibir ninguno, uno o más parámetros
(separados por comas), y pueden o no devolver un resultado.

\begin{figure}[ht]
\begin{center}
\input{graficos/funcion}
\end{center}
\caption{Una función recibe parámetros y devuelve un resultado.}
\end{figure}

\section{Documentación de funciones}

Cada función escrita por un programador realiza una tarea específica.  Cuando
la cantidad de funciones disponibles para ser utilizadas es grande, puede ser
difícil recordar exactamente qué hace cada función.  Es por eso que es
extremadamente importante documentar en cada función cuál es la tarea que
realiza, cuáles son los parámetros que recibe y qué es lo que devuelve, para
que a la hora de utilizarla sea lo pueda hacer correctamente.

Por convención, la documentación de una función se coloca en la primera línea
del cuerpo de la misma, como una cadena de caracteres (que, como vimos en la
sección \ref{instruccion-expresion}, es una instrucción que no tiene ningún
efecto). Dado que la documentación suele ocupar más de una línea de texto,
se acostumbra encerrarla entre tres pares de comillas.

Así, para la función vista en el ejemplo anterior:

\begin{codigo-python-sn}
def hola(alguien):
    """Devuelve un saludo dirigido a la persona indicada por parámetro."""
    return "Hola " + alguien + "! Estoy programando en Python."
\end{codigo-python-sn}

\begin{sabias_que}
Cuando una función definida está correctamente documentada, es posible acceder
a su documentación mediante la función \lstinline!help! provista por Python.
Suponiendo que la función |hola| está definida en el archivo |saludo.py|:

\begin{codigo-python-sn}
>>> import saludo
>>> help(saludo.hola)
Help on function hola in module saludo:

hola(alguien)
    Devuelve un saludo dirigido a la persona indicada por parámetro.
\end{codigo-python-sn}

De esta forma no es necesario mirar el código de una función para saber lo que
hace, simplemente llamando a \lstinline!help! es posible obtener esta
información.

En la sección \ref{sec:modulos} se explica qué hace la instrucción |import|.
\end{sabias_que}

\section{Imprimir versus devolver}

Supongamos que tenemos una medida de tiempo expresada en horas, minutos y
segundos, y queremos calcular la cantidad total de segundos. Cuando nos
disponemos a escribir una función en Python para resolver este problema nos
enfrentamos con dos posibilidades:

\begin{enumerate}
\item \emph{Devolver} el resultado con la instrucción |return|.
\item \emph{Imprimir} el resultado llamando a la función |print|.
\end{enumerate}

A continuación mostramos ambas implementaciones:

\begin{codigo-python-sn}
def devolver_segundos(horas, minutos, segundos):
    """Transforma en segundos una medida de tiempo expresada en
       horas, minutos y segundos"""
    return 3600 * horas + 60 * minutos + segundos

def imprimir_segundos(horas, minutos, segundos):
    """Imprime una medida de tiempo expresada en horas, minutos y
       segundos, luego de transformarla en segundos"""
    print(3600 * horas + 60 * minutos + segundos)
\end{codigo-python-sn}

Veamos si funcionan:

\begin{codigo-python-sn}
>>> devolver_segundos(1, 10, 10)
4210
>>> imprimir_segundos(1, 10, 10)
4210
\end{codigo-python-sn}

Aparentemente el comportamiento de ambas funciones es idéntico, pero hay una
gran diferencia. La función |devolver_segundos| nos permite hacer algo como
esto:

\begin{codigo-python-sn}
>>> s1 = devolver_segundos(1, 10, 10)
>>> s2 = devolver_segundos(2, 32, 20)
>>> s1 + s2
13350
\end{codigo-python-sn}

En cambio, la función |imprimir_segundos| nos impide utilizar el resultado de
la llamada para hacer otras operaciones; lo único que podemos hacer es
mostrarlo en pantalla. Por eso decimos que |devolver_segundos| es más
\emph{reutilizable}. Por ejemplo, podemos reutilizar |devolver_segundos| en la
implementación de |imprimir_segundos|, pero no a la inversa:

\begin{codigo-python-sn}
def imprimir_segundos(horas, minutos, segundos):
    """Imprime una medida de tiempo expresada en horas, minutos y
       segundos, luego de transformarla en segundos"""
    print(devolver_segundos(horas, minutos, segundos))
\end{codigo-python-sn}

Contar con funciones es de gran utilidad, ya que nos permite ir armando una
biblioteca de soluciones a problemas simples, que se pueden reutilizar en la
resolución de problemas más complejos, tal como lo sugiere Thompson en \enquote{How to
program it}.

En este sentido, más útil que tener una biblioteca donde los resultados
se imprimen por pantalla, es contar con una biblioteca donde los
resultados se devuelven, para poder manipular los resultados de esas funciones
a voluntad: imprimirlos, usarlos para realizar cálculos más complejos, etc.

\begin{observacion}
En general, una función es más reutilizable si devuelve un resultado
(utilizando |return|) en lugar de imprimirlo (utilizando |print|).
Análogamente, una función es más reutilizable si recibe parámetros en lugar
de leer datos mediante la función |input|.
\end{observacion}

\ejercicioc{Escribir una función \lstinline+repite_hola+ que reciba como
parámetro un número entero \lstinline+n+ y escriba por pantalla el mensaje
\lstinline!"Hola"!  \lstinline+n+ veces.  Invocarla con distintos valores de
\lstinline+n+.}

\ejercicioc{Escribir otra función \lstinline+repite_hola+ que reciba como
parámetro un número entero \lstinline+n+ y devuelva la cadena formada por
\lstinline+n+ concatenaciones de  \lstinline!"Hola"!. Invocarla con distintos
valores de \lstinline+n+.}

\ejercicioc{Escribir una función \lstinline+repite_saludo+ que reciba como
parámetro un número entero \lstinline+n+ y una cadena \lstinline+saludo+ y
escriba por pantalla el valor de \lstinline+saludo+ \lstinline+n+ veces.
Invocarla con distintos valores de \lstinline+n+ y de \lstinline+saludo+.}

\ejercicioc{Escribir otra función \lstinline+repite_saludo+ que reciba como
parámetro un número entero \lstinline+n+ y una cadena \lstinline+saludo+
devuelva el valor de \lstinline+n+ concatenaciones de \lstinline+saludo+.
Invocarla con distintos valores de \lstinline+n+ y de \lstinline+saludo+.}

\section{Cómo usar una función en un programa}

Las funciones son útiles porque nos permiten repetir la misma operación (puede
que con argumentos distintos) todas las veces que las necesitemos en un
programa, sin tener que reescribir la lista de pasos para realizar la operación
cada vez.

Supongamos que necesitamos un programa que permita transformar tres duraciones
de tiempo en segundos:

\begin{enumerate}

\item {\bf Análisis: } El programa debe pedir al usuario tres duraciones
    expresadas en horas, minutos y segundos, y las tiene que mostrar en
    pantalla expresadas en segundos.

\item {\bf Especificación: }
\begin{itemize}
\item {\bf Entradas: } Tres duraciones leídas de teclado y expresadas en horas,
minutos y segundos.
\item {\bf Salidas: } Mostrar por pantalla cada una de las duraciones
    ingresadas, convertidas a segundos.  Para cada juego de datos de entrada
    ($h$, $m$, $s$) se obtiene entonces $3600 h + 60 m + s$, y se muestra
    ese resultado por pantalla.
\end{itemize}

\item {\bf Diseño:}
\begin{itemize}
\item Se tienen que leer tres conjuntos de datos y para cada conjunto hacer lo
mismo; se trata entonces de un programa con estructura de ciclo definido de
tres pasos:

\begin{codigo-nohl-sn}
repetir 3 veces:
    <hacer cosas>
\end{codigo-nohl-sn}

\item El cuerpo del ciclo (\verb+<hacer cosas>+) tiene la estructura
\emph{Entrada-Cálculo-Salida}.  En pseudocódigo:

\begin{codigo-nohl-sn}
Leer cuántas horas tiene el tiempo dado
 (y referenciarlo con la variable h)

Leer cuántos minutos tiene tiene el tiempo dado
 (y referenciarlo con la variable m)

Leer cuántos segundos tiene el tiempo dado
 (y referenciarlo con la variable s)

Mostrar por pantalla 3600 * h + 60 * m + s
\end{codigo-nohl-sn}

Pero la conversión a segundos es exactamente lo que hace nuestra función
\verb+devolver_segundos+. Si la renombramos a |a_segundos|, podemos hacer que
el cuerpo del ciclo se diseñe como:

\begin{codigo-nohl-sn}
Leer cuántas horas tiene la duración dada
 (y referenciarlo con la variable h)

Leer cuántos minutos tiene tiene la duración dada
 (y referenciarlo con la variable m)

Leer cuántas segundos tiene la duración dada
 (y referenciarlo con la variable s)

(@Invocar la función a_segundos(h, m, s) y@)
(@mostrar el resultado en pantalla.@)
\end{codigo-nohl-sn}

\item El pseudocódigo final queda:

\begin{codigo-nohl-sn}
repetir 3 veces:
    Leer cuántas horas tiene la duración dada
     (y referenciarlo con la variable h)

    Leer cuántos minutos tiene la duración dada
     (y referenciarlo con la variable m)

    Leer cuántos segundos tiene la duración dada
     (y referenciarlo con la variable s)

    Invocar la función a_segundos(h, m, s) y
    mostrar el resultado en pantalla.
\end{codigo-nohl-sn}

\end{itemize}
\item {\bf Implementación:} A partir del diseño, se escribe el programa
Python que se muestra en el Código~\ref{trestiempos}, que se guardará
en el archivo \verb!tres_tiempos.py!.

\begin{codigo}{\label{trestiempos} tres\_tiempos.py}{Lee tres tiempos y los imprime en segundos}
\begin{codigo-python}
def a_segundos(horas, minutos, segundos):
    """Transforma en segundos una medida de tiempo expresada en
       horas, minutos y segundos"""
    return 3600 * horas + 60 * minutos + segundos

def main():
    """Lee tres tiempos expresados en horas, minutos y segundos,
       y muestra en pantalla su conversión a segundos"""
    for x in range(3):
        h = int(input("Cuantas horas?: "))
        m = int(input("Cuantos minutos?: "))
        s = int(input("Cuantos segundos?: "))
        print("Son", a_segundos(h, m, s), "segundos")

main()
\end{codigo-python}
\end{codigo}

\item {\bf Prueba: } Probamos el programa con las ternas $(1,0,0)$, $(0,1,0)$ y
$(0,0,1)$:

\begin{codigo-nohl-sn}
(~\$~) python3 tres_tiempos.py
Cuantas horas?: 1
Cuantos minutos?: 0
Cuantos segundos?: 0
Son 3600 segundos
Cuantas horas?: 0
Cuantos minutos?: 1
Cuantos segundos?: 0
Son 60 segundos
Cuantas horas?: 0
Cuantos minutos?: 0
Cuantos segundos?: 1
Son 1 segundos
\end{codigo-nohl-sn}
\end{enumerate}

\section{Alcance de las variables}

Ya hemos visto que podemos definir variables, ya sea dentro o fuera del cuerpo
de una función. Veamos un ejemplo, utilizando la función |suma_cuadrados| de la
unidad~\ref{chapter:conceptos}:

\begin{codigo-python-sn}
>>> def suma_cuadrados(n):
...     suma = 0
...     for x in range(1, n + 1):
...         suma = suma + cuadrado(x)
...     return suma
>>> y = suma_cuadrados(5)
\end{codigo-python-sn}

¿Qué pasa si intentamos utilizar la variable |suma| fuera de la función?

\begin{codigo-python-sn}
>>> suma
(^Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'suma' is not defined^)
>>>
\end{codigo-python-sn}

\begin{observacion}
Las variables y los parámetros que se declaran dentro de una función no existen
fuera de ella, y por eso se las denomina \emph{variables locales}. Fuera de la
función se puede acceder únicamente al valor que devuelve mediante |return|.
\end{observacion}

Veamos en detalle qué sucede cuando invocamos a la función mediante la
instrucción:

\begin{codigo-python-sn}
>>> y = suma_cuadrados(5)
\end{codigo-python-sn}

\begin{enumerate}
\item Se invoca a |suma_cuadrados| con el argumento 5, y se ejecuta
    el cuerpo de la función con la variable local |n| $\ra$ 5.
\item La función declara una variable local |suma| $\ra$ 0.
\item Cuando la ejecución llega a la línea |return suma|, la variable |suma|
    $\ra$ 55. Por lo tanto, la función devuelve el valor 55.
\item La función termina su ejecución, y con ella dejan de existir todas sus
    variables locales: |n| y |suma|.
\item Se declara la variable |y| $\ra$ 55, que es el valor que devolvió la
    función.
\end{enumerate}

Si la función no devolviera ningún valor, la variable |y| no quedaría asociada
a ningún valor\footnote{Técnicamente, quedaría asociada con un valor especial
llamado \texttt{None}.}.

\section{Un ejemplo completo}

\begin{problemac}
Un usuario nos plantea su problema: necesita que se facture el uso de un teléfono.
Nos informará la tarifa por segundo, cuántas comunicaciones se realizaron,
la duración de cada comunicación expresada en horas, minutos y segundos.
Como resultado deberemos informar la duración en segundos de cada comunicación y
su costo.
\end{problemac}

\begin{solucion}
Aplicaremos los pasos aprendidos:

\begin{enumerate}

\item {\bf Análisis: }
\begin{itemize}
\item ¿Cuántas tarifas distintas se usan? Una sola (la llamaremos $p$).
\item ¿Cuántas comunicaciones se realizaron? La cantidad de comunicaciones (a
la que llamaremos $n$) se informa cuando se inicia el programa.
\item ¿En qué formato vienen las duraciones de las comunicaciones? Vienen como
    ternas $(h, m, s)$.
\item ¿Qué se hace con esas ternas? Se convierten a segundos y se calcula el costo de cada
comunicación multiplicando el tiempo por la tarifa.
\end{itemize}

\item {\bf Especificación: }
\begin{itemize}

\item {\bf Entradas: }
\begin{itemize}
\item Una tarifa $p$ expresada en pesos/segundo.
\item Una cantidad $n$ de llamadas telefónicas.
\item $n$ duraciones de llamadas leídas de teclado y expresadas en horas, minutos y segundos.
\end{itemize}

\item {\bf Salidas: } Mostrar por pantalla las $n$ duraciones ingresadas, convertidas a segundos,
y su costo.
Para cada juego de datos de entrada $(h, m, s)$ se imprime:
$$ 3600 h + 60 m + s $$
$$ p \cdot (3600 h + 60 m + s) $$
\end{itemize}

\item {\bf Diseño:}

Lo primero que hacemos es buscar un programa que haga algo análogo y ver si se
lo puede modificar para resolver nuestro problema.  Hay similitudes entre el requerimiento
y el programa \lstinline+tres_tiempos+ que desarrollamos anteriormente. 
Veamos las diferencias entre sus especificaciones.

\begin{center}
\begin{tabular}{p{0.4\textwidth} p{0.4\textwidth}}
\lstinline!tres_tiempos.py! & \lstinline!tarifador.py!
\\
\hline
\begin{verbatim}
repetir 3 veces:
    <hacer cosas>
\end{verbatim}
&
\begin{verbatim}
leer el valor de p
leer el valor de n
repetir n veces:
    <hacer cosas>
\end{verbatim}
\\
\hline
El cuerpo del ciclo:
{\footnotesize
\begin{verbatim}
Leer el valor de h
Leer el valor de m
Leer el valor de s
Mostrar a_segundos(h, m, s)
\end{verbatim}
} &
El cuerpo del ciclo:
{\footnotesize
\begin{verbatim}
Leer el valor de h
Leer el valor de m
Leer el valor de s
duracion = a_segundos(h, m, s)
costo = duracion * p
Mostrar duracion y costo
\end{verbatim}
} \\
\end{tabular}
\end{center}

\item {\bf Implementación:} El programa resultante se muestra en el Código~\ref{tarifador}.

\begin{codigo}{\label{tarifador} tarifador.py}{Programa para calcular el costo
    de uso de un teléfono.}
\begin{codigo-python}
def main():
    """El usuario ingresa la tarifa por segundo, cuántas
       comunicaciones se realizaron, y la duracion de cada
       comunicación expresada en horas, minutos y segundos. Como
       resultado se informa la duración en segundos de cada
       comunicación y su costo."""

    p = float(input("¿Cuánto cuesta 1 segundo de comunicacion?: "))
    n = int(input("¿Cuántas comunicaciones hubo?: "))
    for x in range(n):
        h = int(input("¿Cuántas horas?: "))
        m = int(input("¿Cuántos minutos?: "))
        s = int(input("¿Cuántos segundos?: "))
        duracion = a_segundos(h, m, s)
        costo = duracion * p
        print("Duracion:", duracion, "segundos. Costo: (~\$~)", costo)

def a_segundos(horas, minutos, segundos):
    """Transforma en segundos una medida de tiempo expresada en
       horas, minutos y segundos"""
	return 3600 * horas + 60 * minutos + segundos

main()
\end{codigo-python}
\end{codigo}

\item {\bf Prueba:} Lo probamos con una tarifa de \$ 0.40 el segundo y tres
ternas de \lstinline!(1,0,0)!, \lstinline!(0,1,0)! y \lstinline!(0,0,1)!:

\begin{codigo-nohl-sn}
(~\$~) python3 tarifador.py
Cuanto cuesta 1 segundo de comunicacion?: 0.40
Cuantas comunicaciones hubo?: 3
Cuantas horas?: 1
Cuantos minutos?: 0
Cuantos segundos?: 0
Duracion: 3600 segundos. Costo: (~\$~) 1440.0
Cuantas horas?: 0
Cuantos minutos?: 1
Cuantos segundos?: 0
Duracion: 60 segundos. Costo: (~\$~) 24.0
Cuantas horas?: 0
Cuantos minutos?: 0
Cuantos segundos?: 1
Duracion: 1 segundos. Costo: (~\$~) 0.4
\end{codigo-nohl-sn}

\item {\bf Mantenimiento:}

\ejercicioc {Corregir el programa para que:
\begin{itemize}
\item Informe el costo en pesos y centavos, en lugar de un número decimal.
\item Informe cuál fue el total facturado en la corrida.
\end{itemize}
}
\end{enumerate}
\end{solucion}

\section{Devolver múltiples resultados}
\label{fun:multiple_return}

\begin{problemac}
Escribir una función que, dada una duración en segundos sin fracciones
(representada por un número entero), calcule la misma duración en horas,
minutos y segundos.
\end{problemac}

\begin{solucion}
La especificación es sencilla:
\begin{itemize}
\item La cantidad de horas es la duración informada en segundos dividida
por 3600 (división entera).
\item La cantidad de minutos es el resto de la división del paso 1,
dividido por 60 (división entera).
\item La cantidad de segundos es el resto de la división del paso 2.
\item Es importante notar que si la duración no se informa como un número
entero, todas las operaciones que se indican más arriba carecen de sentido.
\end{itemize}

¿Cómo hacemos para devolver más de un valor? En realidad lo que se espera
de esta función es que devuelva una terna de valores: si ya calculamos
\lstinline!h!, \lstinline!m! y \lstinline!s!, lo que debemos devolver
es la terna \lstinline+(h, m, s)+:

\begin{codigo-python-sn}
def a_hms(segundos):
   """Dada una duración entera en segundos
      se la convierte a horas, minutos y segundos"""
   h = segundos // 3600
   m = (segundos % 3600) // 60
   s = (segundos % 3600) % 60
   return h, m, s
\end{codigo-python-sn}
\end{solucion}

Esto es lo que sucede al invocar esta función:

\begin{codigo-python-sn}
>>> h, m, s = a_hms(3661)
>>> print("Son", h, "horas", m, "minutos", s, "segundos")
Son 1 horas 1 minutos 1 segundos
\end{codigo-python-sn}

\begin{sabias_que}
Cuando la función debe devolver múltiples resultados, se empaquetan todos juntos
en una \emph{n-upla} (secuencia de valores separados por comas) del tamaño adecuado.

Esta característica está presente en Python, Ruby, Haskell y algunos otros pocos
lenguajes.  En los lenguajes en los que esta característica no está
presente, como C, Pascal o Java, es necesario recurrir a otras
técnicas más complejas para poder obtener un comportamiento similar.
\end{sabias_que}

Respecto de la variable que hará referencia al resultado de la invocación,
se podrá usar tanto una n-upla de variables, como en el ejemplo anterior
(en cuyo caso podremos nombrar en forma separada cada uno de los resultados),
o bien se podrá usar una sola variable (en cuyo caso se considerará que
el resultado tiene un solo nombre y la forma de una n-upla):

\begin{codigo-python-sn}
>>> hms = a_hms(3661)
>>> print(hms)
(1, 1, 1)
\end{codigo-python-sn}

\begin{atencion}
Si se usa una n-upla de variables para referirse a un resultado,
la cantidad de variables tiene que coincidir con la cantidad de valores que
se devuelven.

\begin{codigo-python-sn}
>>> x, y = a_hms(3661)
(^Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: too many values to unpack^)
>>> x, y, w, z = a_hms(3661)
(^Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: need more than 3 values to unpack^)
\end{codigo-python-sn}
\end{atencion}


\section{Módulos}

\label{sec:modulos}A medida que los programas se hacen más grandes y complejos suele ser
conveniente dividirlos en \emph{módulos}. Cada uno de los programas que
escribimos hasta ahora están formados por un único módulo, ya que cada archivo
|.py| es un módulo.

\begin{codigo}{\label{modulo-saludos} saludos.py}{Módulo con funciones para
    saludar}
\begin{codigo-python-sn}
def hola(nombre)
    return "Hola, " + nombre

def adios(nombre)
    return "Adiós, " + nombre
\end{codigo-python-sn}
\end{codigo}

\begin{codigo}{\label{modulo-main} main.py}{Módulo principal del programa}
\begin{codigo-python-sn}
(@import saludos@)

def main()
    nombre = input("¿Cuál es tu nombre?")
    print((@saludos.hola(nombre)@))
    print((@saludos.adios(nombre)@))

main()
\end{codigo-python-sn}
\end{codigo}

En Código~\ref{modulo-saludos} y Código~\ref{modulo-main} se muestra un ejemplo
de un programa formado por dos módulos, |saludos| y |main|:

\begin{itemize}
\item El módulo |saludos| define dos funciones: |hola| y |adios|. Notar que lo
    único que hacemos es definir funciones pero nunca las llamamos, justamente
    porque las vamos a invocar desde el módulo |main|.
\item Lo primero que hacemos en el módulo |main| es utilizar la instrucción de
    Python |import saludos|, para indicar al intérprete que queremos utilizar
    las funciones definidas en el módulo |saludos|. Luego las invocamos, con la
    diferencia de que tenemos que anteceder el nombre de cada función con el
    nombre del módulo y un punto: en este caso |saludos.hola| y |saludos.chau|.
    Y finalmente llamamos a la función |main()|.
\end{itemize}

Para ejecutar el programa lo hacemos con el comando |python main.py|. Cuando el
intérprete encuentre la instrucción |import saludo| automáticamente buscará el
archivo |saludos.py| y lo ejecutará.

\subsection{Módulos estándar}

Se dice que \enquote{Python viene con las baterías incluidas}. Esto es porque el
intérprete incluye un conjunto numeroso de módulos ya implementados con
utilidades de uso general: matemática, acceso al
sistema operativo y la red, depuración, criptografía, compresión, interfaces
gráficas\ldots\ ¡Incluso hay una tortuga!

\begin{sabias_que}
El lenguaje de programación \emph{Logo}, creado en 1967 y utilizado
principalmente con fines educativos, introdujo la idea de crear dibujos
utilizando la metáfora de una \emph{tortuga} que se mueve por la pantalla
obedeciendo a comandos simples.

El módulo |turtle| de Python nos permite crear dibujos usando un sistema muy
similar al de Logo:

\noindent\begin{minipage}{.45\textwidth}
\begin{codigo-python-sn}
import turtle

turtle.shape("turtle")
turtle.color('red', 'yellow')
turtle.begin_fill()
for i in range(5):
    turtle.forward(200)
    turtle.right(144)
turtle.end_fill()
turtle.done()
\end{codigo-python-sn}
\end{minipage}\hfill%
\begin{minipage}{.45\textwidth}
\centerline{\includegraphics[width=0.5\textwidth]{graficos/turtle}}
\end{minipage}
\end{sabias_que}

La lista completa de módulos incluidos y sus respectivas instrucciones de uso
se puede ver en
\url{https://docs.python.org/3/library/index.html}.

\section{Resumen}

\begin{itemize}
\item Una función puede recibir ninguno, uno o más parámetros.
Adicionalmente puede leer datos de la entrada del teclado.
\item Una función puede no devolver nada, o devolver uno o más valores.
Adicionalmente puede imprimir mensajes para
comunicarlos al usuario.
\item No es posible acceder a las variables definidas dentro de una función
desde el programa principal. Si se quiere utilizar algún valor calculado en
la función, será necesario devolverlo.
\item Cuando una función realice un cálculo o una operación,
es preferible que reciba los datos necesarios mediante los parámetros de
la función, y que devuelva el resultado. Las funciones que
leen datos del teclado o imprimen mensajes son menos reutilizables.
\item Es altamente recomendable documentar cada función que se
escribe, para poder saber qué parámetros recibe, qué devuelve y qué
hace sin necesidad de leer el código.
\end{itemize}

\begin{referencia_python}

\begin{sintaxis}{\lstinline!def funcion(param1, param2, param3):!}
Permite definir funciones, que pueden tener ninguno, uno o más
parámetros.  El cuerpo de la función debe estar un nivel de sangría
más adentro que la declaración de la función.

\begin{codigo-python-sn}
def funcion(param1, param2, param3):
    # hacer algo con los parametros
\end{codigo-python-sn}
\end{sintaxis}

\begin{sintaxis}{Documentación de funciones}
Si en la primera línea de la función se ingresa una cadena de
caracteres, la misma por convención pasa a ser la documentación
de la función, que puede ser accedida mendiante el comando
\lstinline!help(funcion)!.
\begin{codigo-python-sn}
def funcion():
    """Esta es la documentación de la función"""
    # hacer algo
\end{codigo-python-sn}
\end{sintaxis}

\begin{sintaxis}{\lstinline!return valor!}
Dentro de una función se utiliza la instrucción \lstinline!return!
para indicar el valor que la función debe devolver.
Una vez que se ejecuta esta instrucción, se termina la ejecución de la
función, sin importar si es la última línea o no.
Si la función no contiene esta instrucción, no devuelve nada.
\end{sintaxis}

\begin{sintaxis}{\lstinline!return valor1, valor2, valor3!}
Si se desea devolver más de un valor, se los {\em empaqueta} en
una n-upla de valores.  Esta n-upla puede o no ser desempaquetada al
invocar la función:
\begin{codigo-python-sn}
def f(valor):
    # operar
    return a1, a2, a3

# desempaquetado:
v1, v2, v3 = f(x)
# empaquetado
v = f(y)
\end{codigo-python-sn}
\end{sintaxis}

\begin{sintaxis}{\lstinline!import modulo!}
Permite utilizar funciones y valores definidos en el módulo especificado.
Las referencias deben ser precedidas por el nombre del módulo y un punto.
\begin{codigo-python-sn}
>>> import math
>>> math.cos(2 * math.pi)
1.0
\end{codigo-python-sn}
\end{sintaxis}

\begin{sintaxis}{\lstinline!import modulo as variable!}
Hace lo mismo que |import modulo|, pero nos permite llamar al módulo con
una variable nombrada por nosotros.
\begin{codigo-python-sn}
>>> import math as matematica
>>> matematica.cos(2 * matematica.pi)
1.0
\end{codigo-python-sn}
\end{sintaxis}

\begin{sintaxis}{\lstinline!from modulo import ref1, ref2, ...!}
Similar a |import modulo|, pero importando únicamente las funciones y valores
especificados, y además eliminando la necesidad de anteponer el nombre del
módulo al utilizarlos:
\begin{codigo-python-sn}
>>> from math import cos, pi
>>> cos(2 * pi)
1.0
\end{codigo-python-sn}
\end{sintaxis}

\end{referencia_python}

\newpage
\section{Ejercicios}

\extractionlabel{guia}
\begin{ejercicio} Escribir dos funciones que permitan calcular:
\begin{partes}
    \item La duración en segundos de un intervalo dado en horas, minutos y segundos.
    \item La duración en horas, minutos y segundos de un intervalo dado en segundos.
\end{partes}
\end{ejercicio}

\extractionlabel{guia}
\begin{ejercicio}
Usando las funciones del ejercicio anterior, escribir un programa que pida al
usuario dos intervalos expresados en horas, minutos y segundos, sume sus
duraciones, y muestre por pantalla la duración total en horas, minutos y segundos.
\end{ejercicio}

\extractionlabel{guia}
\begin{ejercicio}
Escribir una función que, dados cuatro números, devuelva el mayor
producto de dos de ellos. Por ejemplo, si recibe los números 1, 5, -2,
-4 debe devolver 8, que es el producto más grande que se puede obtener
entre ellos ($8 = -2 \times -4$).
\end{ejercicio}

\extractionlabel{guia}
\begin{ejercicio}
{\bf Área de polígonos}
\begin{partes}
    \item Escribir una función que reciba las coordenadas de un vector en
        $\mathbb{R}^3$ (|x,y,z|) y devuelva la norma del vector, dada por
        $\lVert\overrightarrow{(x,y,z)}\rVert=\sqrt{x^2+y^2+z^2}$. \\
        Ejemplo: |norma(3, 2, -4)| $\rightarrow$ |5.3851|

    \item Escribir una función que reciba las coordenadas de dos vectores en
        $\mathbb{R}^3$ (|x1,y1,z1,x2,y2,z2|) y devuelva las coordenadas del vector
        diferencia (debe devolver 3 valores numéricos). \\
        Ejemplo: |diferencia(8, 7, -3, 5, 3, 2)| $\rightarrow$ |(3, 4, -5)|

    \item Escribir una función que reciba las coordenadas de dos vectores en
        $\mathbb{R}^3$ y devuelva las coordenadas del producto vectorial,
        definido como:
        $$\overrightarrow{(x_1, y_1, z_1)} \times \overrightarrow{(x_2, y_2, z_2)} =
        \overrightarrow{(y_1 z_2 - z_1 y_2, \; z_1 x_2 - x_1 z_2, \; x_1 y_2 - y_1 x_2)}$$
        Ejemplo: |producto_vec(1, 4, -2, 3, -1, 0)| $\rightarrow$ |(-2, -6, -13)|

    \item Utilizando las funciones anteriores, escribir una función que reciba
        las coordenadas de 3 puntos en $\mathbb{R}^3$ y devuelva el área del triángulo
        que conforman.\\
        \emph{Ayuda}: Si $A$, $B$ y $C$ son 3 puntos en el espacio, la norma del producto
        vectorial $\overrightarrow{AB} \times \overrightarrow{AC}$ es igual al doble del área del
        triángulo $\stackrel{\vartriangle}{ABC}$. \\
        Ejemplo: |area_triangulo(5, 8, -1, -2, 3, 4, -3, 3, 0)| $\rightarrow$ |19.4551|

    \item Escribir una función que reciba las coordenadas de 4 puntos en el
        plano $\mathbb{R}^2$ (|x1,y1,x2,y2,x3,y3,x4,y4|) que conforman un cuadrilátero
        convexo, y devuelva el área del mismo. \\
        \emph{Ayuda}: Aprovechar las funciones escritas anteriormente, asumiendo
        que los puntos dados están en $\mathbb{R}^3$ con coordenada $z = 0$. \\
        Ejemplo: |area_cuadrilatero(4, 3, 5, 10, -2, 8, -3, -5)| $\rightarrow$
        |65|
\end{partes}
\end{ejercicio}
