\chapter{Diccionarios}

En esta unidad analizaremos otra estructura de datos importante: los diccionarios.
Su importancia radica no sólo en las grandes posibilidades que presentan
como estructuras para almacenar información, sino también en que, en
Python, son utilizados por el propio lenguaje para realizar diversas
operaciones y para almacenar información de otras estructuras.

\section{Qué es un diccionario}

Según Wikipedia, \enquote{[u]n diccionario es una obra de consulta de
palabras y/o términos que se encuentran generalmente ordenados
alfabéticamente. De dicha compilación de palabras o términos se
proporciona su significado, etimología, ortografía y, en el caso
de ciertas lenguas fija su pronunciación y separación silábica.}


Al igual que los diccionarios a los que se refiere Wikipedia,
los diccionarios de Python son una colección de términos (llamados
\emph{claves}) asociados a un \emph{valor} determinado. A diferencia de los
diccionarios a los que se refiere Wikipedia, el orden en los diccionarios de
Python no es relevante.

Dicho de otra manera, un diccionario es una colección de pares $(clave,
valor)$. A diferencia de las listas y tuplas, en lugar de acceder a un valor
mediante un índice numérico, el acceso será a través de su clave, que puede
ser de diversos tipos.

\begin{figure}[hbt]
\begin{tikzpicture}[
    every node/.style={node font=\ttfamily},
]
    \node [] (k1) {"ar"};
    \node [below=0.5em of k1] (k2) {"es"};
    \node [below=0.5em of k2] (k3) {"tv"};
    \node [right=of k1] (v1) {"Argentina"};
    \node [right=of k2] (v2) {"España"};
    \node [right=of k3] (v3) {"Tuvalu"};
    \path [flecha] (k1) -- (v1);
    \path [flecha] (k2) -- (v2);
    \path [flecha] (k3) -- (v3);

    \node [fit=(k1) (k2) (k3)] (claves) {};
    \node [above=0cm of claves.north,anchor=south] (lclaves) {$claves$};

    \node [fit=(v1) (v2) (v3)] (valores) {};
    \node [above=0cm of valores.north,anchor=south] (lvalores) {$valores$};

    \node [bloque,fit=(claves) (lclaves) (valores) (lvalores)] (caja) {};
\end{tikzpicture}
\caption{Un diccionario cuyas claves son dominios de Internet (\emph{top level
    domains}) y cuyos valores son los países correspondientes.}
\end{figure}

Las claves son únicas dentro de un diccionario, es decir que no puede haber
un diccionario que tenga dos veces la misma clave. Si se asigna un valor a
una clave ya existente, se reemplaza el valor anterior.

No hay una forma directa de acceder a una clave a través de su valor, y
nada impide que un mismo valor se encuentre asignado a distintas claves.

Dado que el orden en los diccionarios no es relevante, dos diccionarios
se consideran iguales si contienen las mismas claves asociadas a los mismos
valores, incluso aunque los elementos hayan sido agregados en diferente orden.

Al igual que las listas, los diccionarios son mutables. Esto significa que
podemos agregar, quitar y modificar los elementos de un diccionario
posteriormente a su creación.

Cualquier valor de tipo inmutable puede ser clave de un diccionario:
cadenas, enteros, tuplas (con valores inmutables en sus miembros), etc.  No hay
restricciones para los valores que el diccionario puede contener, cualquier
tipo puede ser el valor: listas, cadenas, tuplas, otros diccionarios,
etc.

\begin{sabias_que}
En otros lenguajes de programación, a los diccionarios se los llama \emph{arreglos asociativos},
\emph{mapas} o \emph{tablas}.
\end{sabias_que}

\section{Utilizando diccionarios en Python}

De la misma forma que con listas, es posible definir un diccionario
directamente con los miembros que va a contener, o bien inicializar el
diccionario vacío y luego agregar los valores de a uno o de a muchos.

Para definirlo junto con los miembros que va a contener, se encierra el
listado de valores entre llaves, las parejas de clave y valor se separan
con comas, y la clave y el valor se separan con ':'.

\begin{codigo-python-sn}
dominios = {'ar': 'Argentina', 'es': 'España', 'tv': 'Tuvalu'}
\end{codigo-python-sn}

\begin{observacion}
En Python el tipo de dato asociado a los diccionarios se llama |dict|:

\begin{codigo-python-sn}
>>> type(dominios)
<class 'dict'>
\end{codigo-python-sn}
\end{observacion}

Para declararlo vacío y luego ingresar los valores, se lo declara como un
par de llaves sin nada en medio, y luego se asignan valores directamente a
los índices.

\begin{codigo-python-sn}
materias = {}
materias["lunes"] = [6103, 7540]
materias["martes"] = [6201]
materias["miércoles"] = [6103, 7540]
materias["jueves"] = []
materias["viernes"] = [6201]
\end{codigo-python-sn}

Para acceder al valor asociado a una determinada clave, se lo hace
de la misma forma que con las listas, pero utilizando la clave
elegida en lugar del índice.

\begin{codigo-python-sn}
>>> materias["lunes"]
[6103, 7540]
\end{codigo-python-sn}

\begin{atencion}
El acceso por clave falla si se provee una clave que no está en el diccionario:

\begin{codigo-python-sn}
>>> materias["domingo"]
(^Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
KeyError: 'domingo'^)
\end{codigo-python-sn}
\end{atencion}

El operador |in| nos permite preguntar si una clave se encuentra o no en el
diccionario:

\begin{codigo-python-sn}
>>> "lunes" in materias
True
>>> "domingo" in materias
False
\end{codigo-python-sn}

Además podemos utilizar la función \lstinline{get}, que recibe una
clave $k$ y un valor por omisión $v$, y devuelve el valor asociado a la clave
$k$, en caso de existir, o el valor $v$ en caso contrario.

\begin{codigo-python-sn}
>>> materias.get("lunes", [])
[6103, 7540]
>>> materias.get("domingo", [])
[]
\end{codigo-python-sn}

Existen diversas formas de iterar los elementos de un diccionario. Por ejemplo,
es posible iterar por sus claves y usar esas claves para acceder a los valores:

\begin{codigo-python-sn}
for dia in materias:
    print(f"El {dia} tengo que cursar {materias[dia]}")
\end{codigo-python-sn}

Es posible, también, obtener los valores como tuplas donde el primer
elemento es la clave y el segundo el valor.

\begin{codigo-python-sn}
for dia, codigos in materias.items():
    print(f"El {dia} tengo que cursar {codigos}"
\end{codigo-python-sn}

Recordar que el orden de los elementos no es relevante; por lo tanto no podemos
asumir que el resultado de la iteración saldrá en ningún orden
particular\footnote{En versiones recientes de Python el orden de iteración
corresponde al orden en que los elementos fueron añadidos al diccionario; en
las versiones anteriores no se daba ninguna garantía acerca del orden de
iteración.}. Además, no es posible obtener porciones de un diccionario
usando \lstinline![:]!.

Hay muchas otras operaciones que se
pueden realizar sobre los diccionarios, que permiten manipular la información
según sean nuestras necesidades. Algunos de estos métodos pueden verse en
la referencia al final de la unidad.

\begin{sabias_que}
En la sección \ref{lookup-listas} mencionamos que Python garantiza que para
cualquier lista |L| con $N$ elementos se cumple que |L[i]| es una operación de
\emph{tiempo constante}, sin importar el valor de $N$ o de |i|.

Los diccionarios en Python tienen la misma propiedad: para cualquier
diccionario |D| con $N$ pares clave-valor, y para cualquier clave |k|, la
operación |D[k]| es de tiempo constante.

Dado que las claves pueden ser de cualquier tipo (a diferencia de las listas, en
las que los índices son números enteros entre 0 y $N-1$), para garantizar esta
propiedad, el algoritmo utilizado para almacenar los datos en el diccionario
debe ser más sofisticado que el utilizado para las listas.

Los diccionarios de Python están implementados usando una estructura de datos
llamada \emph{tabla de hash}. Para cada clave se calcula un valor numérico
mediante un algoritmo llamado \emph{código de hash}, que produce valores
muy dispares dependiendo de la clave.  Por ejemplo, el hash de la cadena
|"Python"| es -539294296 mientras que el de |"python"|, una cadena que
difiere en un caracter, es 1142331976. Los pares clave-valor del diccionario
se guardan internamente en una lista, y el código de hash de la clave se
utiliza para determinar el índice en la lista donde se ubicará cada par.
\end{sabias_que}

\section{Algunos usos de diccionarios}

Los diccionarios son una herramienta muy versátil.  Se puede utilizar un
diccionario, por ejemplo, para contar cuántas apariciones de cada palabra
hay en un texto, o cuántas apariciones de cada letra.

Es posible utilizar un diccionario, también, para tener una agenda donde la
clave es el nombre de la persona, y el valor es una lista con los datos
correspondientes a esa persona.

También podría utilizarse un diccionario para mantener los datos de los
alumnos inscriptos en una materia, siendo la clave el número de padrón, y
el valor una lista con todas las notas asociadas a ese alumno.

En general, los diccionarios sirven para crear bases de datos muy simples,
en las que la clave es el identificador del elemento, y el valor son todos
los datos del elemento a considerar.

Otro posible uso de un diccionario sería para realizar
traducciones, donde la clave sería la palabra en el idioma original y el
valor la palabra en el idioma al que se quiere traducir.  Sin embargo esta
aplicación es poco destacable, ya que esta forma de traducir suele dar
resultados poco satisfactorios.

\section{Resumen}

\begin{itemize}
\item Los diccionarios son una estructura de datos
muy poderosa, que permite almacenar un conjunto de pares $clave \rightarrow valor$.
\item Las claves deben ser inmutables y únicas.
\item Los valores pueden ser de cualquier tipo, y pueden no ser únicos.
\item El orden de los elementos no es relevante.
\end{itemize}

\begin{referencia_python}

\begin{sintaxis}{\lstinline!\{clave1:valor1, clave2:valor2\}!}
Se crea un nuevo diccionario con los valores asociados a las claves.  Si no
se ingresa ninguna pareja de clave y valor, se crea un diccionario vacío.
\end{sintaxis}

\begin{sintaxis}{\lstinline{diccionario[clave]}}
Accede al valor asociado con \lstinline!clave! en el diccionario. Falla si la
clave no está en el diccionario.
\end{sintaxis}

\begin{sintaxis}{\lstinline{clave in diccionario}}
Indica si un diccionario tiene o no una determinada clave.
\end{sintaxis}

\begin{sintaxis}{\lstinline{diccionario.get(clave, valor_predeterminado)}}
Devuelve el valor asociado a la clave.  A diferencia del acceso directo
utilizando \lstinline{[clave]}, en el caso en que el valor no se
encuentre devuelve el |valor_predeterminado|.
\end{sintaxis}

\begin{sintaxis}{\lstinline{for clave in diccionario:}}
Permite recorrer una a una todas las claves almacenadas en
el diccionario.
\end{sintaxis}

\begin{sintaxis}{\lstinline{diccionario.keys()}}
Devuelve una secuencia con todas las claves que se hayan ingresado
al diccionario.
\end{sintaxis}

\begin{sintaxis}{\lstinline{diccionario.values()}}
Devuelve una secuencia con todos los valores que se hayan
ingresado al diccionario.
\end{sintaxis}

\begin{sintaxis}{\lstinline{diccionario.items()}}
Devuelve una secuencia con tuplas de dos elementos, en las que el
primer elemento es la clave y el segundo el valor.
\end{sintaxis}

\begin{sintaxis}{\lstinline{diccionario.pop(clave)}}
Quita del diccionario la clave y su valor asociado, y devuelve el valor.
\end{sintaxis}
\end{referencia_python}


\newpage
\section{Ejercicios}

\extractionlabel{guia}
\begin{ejercicio}
Escribir una función que reciba una lista de tuplas, y que devuelva
un diccionario en donde las claves sean los primeros elementos de las
tuplas, y los valores una lista con los segundos.

Por ejemplo:
\begin{lstlisting}[numbers=none]
>>> l = [ ('Hola', 'don Pepito'), ('Hola', 'don Jose'),
          ('Buenos', 'días') ]
>>> print(tuplas_a_diccionario(l))
{ 'Hola': ['don Pepito', 'don Jose'], 'Buenos': ['días'] }
\end{lstlisting}
\end{ejercicio}

\extractionlabel{guia}
\begin{ejercicio}
{\bf Diccionarios usados para contar.}
\begin{partes}
  \item Escribir una función que reciba una cadena y devuelva un diccionario con
la cantidad de apariciones de cada palabra en la cadena.  Por ejemplo, si
recibe "Qué lindo día que hace hoy" debe devolver:
\lstinline!{ 'que': 2, 'lindo': 1, 'día': 1, 'hace': 1, 'hoy': 1}!.

  \item Escribir una función que cuente la cantidad de apariciones de cada
caracter en una cadena de texto, y los devuelva en un diccionario.

  \item Escribir una función que reciba una cantidad de iteraciones de una tirada
de 2 dados a realizar y devuelva la cantidad de veces que se observa cada valor
de la suma de los dos dados. \\
{\bf Nota}: utilizar el módulo \verb!random! para obtener tiradas aleatorias.
\end{partes}
\end{ejercicio}

\extractionlabel{guia}
\begin{ejercicio}
{\bf Continuación de la agenda.} \\
Escribir un programa que vaya solicitando al usuario que ingrese nombres.
\begin{partes}
  \item Si el nombre se encuentra en la agenda (\emph{implementada con un
diccionario}), debe mostrar el teléfono y, opcionalmente, permitir
modificarlo si no es correcto.
  \item Si el nombre no se encuentra, debe permitir ingresar el teléfono
correspondiente.
\end{partes}
El usuario puede utilizar la cadena "*", para salir del programa.
\end{ejercicio}

\extractionlabel{guia}
\begin{ejercicio}
Escribir una función que reciba un texto y para cada caracter presente en el
texto devuelva la cadena más larga en la que se encuentra ese caracter.
\end{ejercicio}

\newpage
\begin{subappendices}
\section{Conjuntos}

Supongamos que queremos modelar un registro de donantes de órganos.
Este registro se comporta como un \emph{conjunto} de elementos, donde cada
elemento es una persona, y el conjunto:

\begin{enumerate}
    \item no puede contener elementos repetidos: una persona puede ser donante
        o no, pero no puede figurar dos o más veces en el registro.
    \item debe permitir averiguar si un elemento pertenece o no al conjunto en
        \emph{tiempo constante}: no importa si hay 1, 10 o 100000 donantes,
        queremos tener la capacidad de averiguar si una persona determinada
        es donante o no rápidamente.
\end{enumerate}

Si implementáramos el registro usando una lista de Python, nos encontraríamos
con que no cumplimos con los requisitos:

\begin{enumerate}
    \item La lista puede contener elementos repetidos. Si bien podemos salvar este
        detalle preguntando si el elemento se encuentra o no en la lista antes
        de agregarlo, esto sería muy poco eficiente, ya que:
    \item Como vimos en la Sección~\ref{busqueda-lineal}, buscar un elemento en la
        lista consume una cantidad de tiempo proporcional a la cantidad de
        elementos presentes en la lista, con lo que no es posible cumplir con
        el requerimiento de tiempo constante.
\end{enumerate}

Una solución posible es usar un diccionario, guardando como clave el número de
documento de la persona donante, y como valor\ldots\ ¡cualquier cosa! No
importa qué vayamos asignar como valor, ya que lo único que queremos aprovechar
del diccionario es la capacidad de tener claves únicas y poder preguntar en
tiempo constante si una clave está o no presente. Por ejemplo, si usamos |True|
como valor:

\begin{codigo-python-sn}
>>> donantes = {12345: True, 23456: True}
>>> 23456 in donantes
True
>>> 34567 in donantes
False
>>> donantes.pop(23456)
>>> 23456 in donantes
False
\end{codigo-python-sn}

Sin embargo, utilizar un diccionario para modelar un conjunto de elementos no
es la solución más elegante. Hay una mejor forma de hacerlo, que es
utilizando el tipo de datos |set|\footnote{La palabra \enquote{set} significa
\enquote{conjunto} en inglés.}.

Para crear un |set| usamos la sintaxis |{<expresión>, <expresión>, ...}|:

\begin{codigo-python-sn}
>>> donantes = {12345, 23456}
>>> type(donantes)
<class 'set'>
>>> 23456 in donantes
True
>>> 34567 in donantes
False
>>> donantes.remove(23456)
>>> 23456 in donantes
False
\end{codigo-python-sn}

Un |set| es una estructura de datos mutable (como las listas y los
diccionarios), que permite agregar y quitar elementos cumpliendo los requisitos de
unicidad y búsqueda en tiempo constante. Además es posible hacer operaciones
entre |set|s como unión, intersección y diferencia muy fácilmente:

\begin{codigo-python-sn}
>>> s1 = {1, 2, 3, 4}
>>> s1
{1, 2, 3, 4}
>>> s1.add(1)
>>> s1
{1, 2, 3, 4}
>>> s2 = {3, 4, 5, 6}
>>> s1.union(s2)
{1, 2, 3, 4, 5, 6}
>>> s1.intersection(s2)
{3, 4}
>>> s1.difference(s2)
{1, 2}
\end{codigo-python-sn}

Notar que la sintaxis para crear un conjunto es muy similar a la de creación de
diccionarios. El caso especial es cuando queremos crear un conjunto vacío: la
sintaxis |{}| no funcionará, ya que eso crea un diccionario vacío. Podemos
crear un conjunto vacío con: |set()|.

\begin{codigo-python-sn}
>>> type({})
<class 'dict'>
>>> type(set())
<class 'set'>
\end{codigo-python-sn}

La referencia completa del tipo de dato |set| puede verse en
\url{https://docs.python.org/3/library/stdtypes.html#set}.
\end{subappendices}
